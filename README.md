Libraries in C:\Users\fcoelho\.platformio\lib\TFT_eSPI_ID1559

Carefull, if screen stops to work check if the UserSetup.h goes to original.

## Logging
To get a good logging it is necessary to configure some things.
Check platformio.ini to set default logging level.

Check esp32-hal-loh.h to change format of logging.
#define ARDUHAL_LOG_FORMAT(letter, format) ARDUHAL_LOG_COLOR_##letter "[" #letter "][%d][%s:%u] %s(): " format ARDUHAL_LOG_RESET_COLOR "\r\n", esp_log_timestamp(), pathToFileName(__FILE__), __LINE__, __FUNCTION__

still trying to get the right format but good for now.

Logging in time sensitive applications seems to slow.
For example in DALI, it completly broke the response times making the communication impossible.



## compiling instruction
in lv_conf.h
// change by Filipe
#define LV_USE_USER_DATA        1


## For m5stack: 
uses TFT_eSPI. it is necessary to go to .pio\libdeps\m5stack\TFT_eSPI\User_Setup_Select.h
and select the M5stack configurations.

