const DEBUG_CONSOLE = false;

var socket = null;  // will hold websocket object

function remove_non_ascii(str) {

    if ((str === null) || (str === ''))
        return '';
    else
        str = str.toString();

    return str.replace(/[^\x08-\x7E]/g, '');
}

function connect() {
    var console_wb_loc = "ws://" + window.location.host + "/console";
    socket = new WebSocket(console_wb_loc);

    socket.onopen = function (event) {
        console.log("Websocket openned!")
    }

    socket.onmessage = function (event) {
        if (DEBUG_CONSOLE) {
            console.log("WebSocket message received:", event.data);
        }

        var data = remove_non_ascii(event.data);

        var $output = $("#incomming_textarea");
        var val = $output.val();
        $output.val(val + data);
        if ($("#scroll_checkbox").prop('checked')) {
            $output.scrollTop($output[0].scrollHeight);
        };
    };

    socket.onclose = function (e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function () {
            connect();
        }, 1000);
    };

    socket.onerror = function (err) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        socket.close();
    };


}
function send_data() {
    let ongoing_message = $("#outgoing_textarea").val();
    if (DEBUG_CONSOLE) {
        console.log("Ongoing_message: " + ongoing_message);
    }
    if (socket != null) {
        socket.send(ongoing_message);
        // clear text box
        $("#outgoing_textarea").val("");
    }
    else {
        alert("Socket not ready!");
    }
}

function send_button_clicked() {
    send_data();
}

function clear_button_clicked() {
    console.log("clearing test area");
    $("#incomming_textarea").val("");
}

function out_on_input(text) {
    //console.log(text);
    let send_on_enter = $("#enter_checkbox").prop('checked');

    if (send_on_enter) {
        let text = $("#outgoing_textarea").val();
        if (text[text.length - 1] == '\n') {
            console.log("send on enter");
            send_data();
        }
    }
}

connect();