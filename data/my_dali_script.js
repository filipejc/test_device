var is_running = false;     // this variable is used to control the status of receiving messages
var scroll_on_data = true;  // scroll on data received
var message_number = 1;     // store number of the message

const DEBUG_TABLE = false;  // set true to see html to create table
var id = 0;

console.log("my_dali_script.js loaded!");

function connect() {
    var dali_wb_loc = "ws://" + window.location.host + "/dali";
    //var dali_wb_loc = "ws://localhost:8080/";
    console.log("Dali websocket location: " + dali_wb_loc);

    var socket = new WebSocket(dali_wb_loc, 'echo-protocol');

    socket.onopen = function (event) {
        console.log("Websocket openned!")
    }

    socket.onmessage = function (event) {
        //console.log("WebSocket message received:", event.data);
        if (!is_running)
            return;

        try {
            var obj = JSON.parse(event.data);
        }
        catch (err) {
            console.log(err.message);
            console.log("Message with error received:", event.data);
            return;
        }

        if (obj["type"] === "dali") {
            var date = new Date();
            var timeString = date.toISOString().substr(11, 8);

            var row = "<tr><th scope=\"row\">" + message_number + "</th><td>" +
                timeString + "</td><td>" +
                obj.len + "</td><td>" +
                obj.dali_type + "</td><td>" +
                obj.hex_data + "</td></tr>";

            if (DEBUG_TABLE)
                console.log(row);

            $('#dali_table').append(row);
            message_number += 1;

            // scroll down
            if (scroll_on_data) {
                $(".table-responsive").animate({ scrollTop: $(".table-responsive")[0].scrollHeight }, 10);
            }
        }
    };

    socket.onclose = function (e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function () {
            connect();
        }, 1000);
    };

    socket.onerror = function (err) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        socket.close();
    };
}

function play_clicked() {
    console.log("Play clicked!");
    if (!is_running) {
        is_running = true;
        console.log("Change icons to start playing");
        $("#play_button").attr("src", "./img/play-fill.svg");
        $("#pause_button").attr("src", "./img/pause.svg");
    }
}

function pause_clicked() {
    console.log("Pause clicked!");
    if (is_running) {
        is_running = false;
        $("#pause_button").attr("src", "./img/pause-fill.svg");
        $("#play_button").attr("src", "./img/play.svg");
    }
}


function stop_clicked() {
    console.log("Stop clicked!");
    //$("#stop_button").attr("src", "./img/x-square-fill.svg");
    // $("#incomming_textarea").val("");
    $("#dali_table tbody").html("");
    message_number = 1;
}
function scroll_clicked() {
    console.log("Scroll clicked!");
    scroll_on_data = !scroll_on_data;
    if (scroll_on_data) {
        $("#scroll_button").attr("src", "./img/arrow-down-circle-fill.svg");
    } else {
        $("#scroll_button").attr("src", "./img/arrow-down-circle.svg");
    }
}


function save_clicked() {
    console.log("Save clicked!");
}

connect();