console.log("other.js loaded!")
const DEBUG_MESSAGES = false;

function connect() {
    var dali_wb_loc = "ws://" + window.location.host + "/dali";
    console.log("Websocket location: " + dali_wb_loc);

    var socket = new WebSocket(dali_wb_loc);

    socket.onopen = function (event) {
        console.log("Websocket openned!")
    }

    var id = 0;

    socket.onmessage = function (event) {
        // console.log("WebSocket message received:", event.data);

        var obj = JSON.parse(event.data);
        if (DEBUG_MESSAGES) {
            console.log(obj);
        }

        if (obj.type == "1_10V") {
            $('#1_10v_label').text("Volt: " + obj.volt_1_10V.toFixed(2) + "V, percentage: " + obj.votl_1_10V_perc.toFixed(1) + "%");
            var perc_int_1_10v = Math.floor(obj.votl_1_10V_perc);
            $("#1-10v_bar").css("width", perc_int_1_10v + "%");
            $("#1-10v_bar").attr("aria-valuenow", perc_int_1_10v);
        }
        else if (obj.type == "LSI_state") {
            if (obj.state == "1") {
                $("#LSI_state").attr("src", "./img/circle-fill.svg");
            } else if (data == "0") {
                $("#LSI_state").attr("src", "./img/circle.svg");
            }
        }
        else if (obj.type == "dali_change") {
            if (obj.dali_type == "light_level") {
                var index = obj.index + 1;
                console.log("Driver: " + index + ", level: " + obj.data);
                var dali_value_int = Math.floor(obj.data * 100 / 254);
                $("#dali_light_" + index).css("width", dali_value_int + "%");
                $("#dali_light_" + index).attr("aria-valuenow", dali_value_int);
                // change label
                $("#dali_label_" + index).text("Dali value: " + obj.data);

            }
            console.log("Driver status" + obj);
        }
        else if (obj.type == "dali") {
            // just to cover this case
        } else if (obj.type == "Light") {
            $('#light_label').text("Light: " + obj.lux + " lux");
        }
        else {
            console.log("Unrecognized type:" + obj.type);
        }
    };

    socket.onclose = function (e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function () {
            connect();
        }, 1000);
    };

    socket.onerror = function (err) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        socket.close();
    };
}


function simulate_failure_func_1() {
    console.log("Simulate failure in Dali on driver number #1");

    var state = $("#simulate_failure_1").is(":checked");

    // ajax request to server
    $.post("/simulate_failure",
        {
            state: (state ? 1 : 0),
            driver: 0
        },
        function (data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
}

function simulate_failure_func_2() {
    console.log("Simulate failure in Dali driver #2");
    var state = $("#simulate_failure_2").is(":checked");

    // ajax request to server
    $.post("/simulate_failure",
        {
            state: (state ? 1 : 0),
            driver: 1
        },
        function (data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
}

function myFunction() {
    console.log("checkbox pressed!");
    var checkBox = $("#relay_checkbox");
    var state = checkBox.is(":checked");
    // If the checkbox is checked, display the output text
    if (state) {
        console.log("Set relay true");
    } else {
        console.log("Set relay false");
    }

    // ajax request to server
    $.post("/on_off",
        {
            state: (state ? 1 : 0)
        },
        function (data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
            if (data == "1") {
                $("#relay_state").attr("src", "./img/circle-fill.svg");
            } else if (data == "0") {

                $("#relay_state").attr("src", "./img/circle.svg");
            } else {
                console.log("Not recognized data");
            }
        });
}


function callback_1_10v() {
    console.log("1-10V checkbox pressed!");
    var checkBox = $("#checkbox_1_10v");
    var state = checkBox.is(":checked");
    // If the checkbox is checked, display the output text
    if (state) {
        console.log("Set 1-10v true");
    } else {
        console.log("Set 1-10v false");
    }

    // ajax request to server
    $.post("/1_10v_state",
        {
            state: (state ? 1 : 0)
        },
        function (data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
            if (data == "1") {
                $("#state_1_10v").attr("src", "./img/circle-fill.svg");
            } else if (data == "0") {

                $("#state_1_10v").attr("src", "./img/circle.svg");
            } else {
                console.log("Not recognized data");
            }
        });
}


function my_LSI_set_function() {
    console.log("Set LSI function");
    var checkBox = $("#lsi_set_checkbox");
    var state = checkBox.is(":checked");
    // If the checkbox is checked, display the output text
    if (state) {
        console.log("Set LSI true");
    } else {
        console.log("Set LSI false");
    }
    // ajax request to server
    $.post("/din_state",
        {
            state: (state ? 1 : 0)
        },
        function (data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
            if (data == "1") {
                $("#LSI_state").attr("src", "./img/circle-fill.svg");
            } else if (data == "0") {

                $("#LSI_state").attr("src", "./img/circle.svg");
            } else {
                console.log("Not recognized data");
            }
        });
}


connect();