
#elif TFT_1_8
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <FS.h>
#include <SD.h>
#include <SPIFFS.h>
#include <SPI.h>

#include <ClickEncoder.h>

#define ENCODER_STEPS 4

// ISR for digital in. IRAM_ATTR used to include this code in IRAM (internal RAM)
void IRAM_ATTR onTimer(); // Start the timer to read the clickEncoder every 1 ms

#include <menu.h>
#include <menuIO/TFT_eSPIOut.h>
#include <streamFlow.h>
#include <menuIO/clickEncoderIn.h>
#include <menuIO/keyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialIO.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>

// TODO: implement my own button debouncer. Current solution only register, every second press.
// it can  be related taht the debouconce time only elapse after the call to ISR, use a time or put the tick in the screen task
// ESP32 timer
// ESP32 timer thanks to: http://www.iotsharing.com/2017/06/how-to-use-interrupt-timer-in-arduino-esp32.html
// and: https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
hw_timer_t *timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// create encoder
ClickEncoder encoder = ClickEncoder(CLK_PINNUMBER, DATA_PIN_NUMBER, SW_PINNUMBER, ENCODER_STEPS);
ClickEncoderStream encStream(encoder, 1);

// // ESP32 timer
void setup_encoder()
{
    Serial.println("Initializing my encoder");
    encoder.setAccelerationEnabled(true);
    encoder.setDoubleClickEnabled(false);

    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &onTimer, true);
    timerAlarmWrite(timer, 1000, true);
    timerAlarmEnable(timer);
}

// service the encoder
void IRAM_ATTR onTimer()
{
    encoder.service();
}

TFT_eSPI gfx = TFT_eSPI(); // Invoke library, pins defined in User_Setup.h

#define TIME_X 70
#define TIME_Y 1
#define TIME_FONT 2
#define MENU_X 20
#define MENU_Y 1
#define MENU_FONT 2

#define MAX_DEPTH 3

using namespace Menu;

// workarround to know in which menu the program are
bool in_1_10v_menu = false;
bool in_dali_menu = false;

result set_1_10v_on()
{
    driver_1_10v->set_state(true);
    return proceed;
}

result set_1_10v_off()
{
    driver_1_10v->set_state(false);
    return proceed;
}

result led_change(eventMask e, prompt &p)
{
    // Serial.println(p.);
    Serial.println("Led change!");
    return proceed;
};

result led_on()
{
    Serial.println("Led on!");
    return proceed;
}

result led_off()
{
    Serial.println("Led off!");
    return proceed;
}

#define RECT_POSX 2
#define RECT_POSY 135
#define RECT_HEIGHT 20
#define RECT_WIDTH 124
#define RECT_RADIUS 2

#define STRING_POSX_3 2
#define STRING_POSY_3 95
#define STRING_POSX_2 2
#define STRING_POSY_2 115

#define STRING_LABEL_COLOR TFT_WHITE
#define STRING_VALUE_COLOR TFT_YELLOW

void print_text(int32_t pos_x, int32_t pos_y, String label, String value)
{
    gfx.setTextColor(TFT_WHITE, TFT_BLACK);
    gfx.drawString(label, pos_x, pos_y);
    int pixel_size = gfx.textWidth(label);
    gfx.setTextColor(STRING_VALUE_COLOR, TFT_BLACK);
    gfx.drawString(value, pos_x + pixel_size, pos_y);
}

void print_percentage_bar(int32_t pos_x, int32_t pos_y, int32_t width, int32_t height, float percentage)
{
    int width_percentage = (int)(percentage * (width - 2) / 100);
    gfx.drawRoundRect(pos_x, pos_y, width, height, RECT_RADIUS, TFT_WHITE);
    gfx.fillRect(pos_x + 1, pos_y + 1, width_percentage, height - 2, TFT_RED);
    gfx.fillRect(pos_x + 2 + width_percentage, pos_y + 1, width - width_percentage - 3, height - 2, TFT_BLACK);
}

void delete_percentage_bar(int32_t pos_x, int32_t pos_y, int32_t width, int32_t height)
{
    gfx.fillRect(pos_x, pos_y, width, height, TFT_BLACK);
}

result print_1_10V(eventMask e, prompt &p)
{
    if (e == enterEvent)
    {
        in_1_10v_menu = true;
        print_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT, 0);
    }
    else if (e == exitEvent)
    {
        in_1_10v_menu = false;
        delete_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT);
    }
    return proceed;
}

result print_dali(eventMask e, prompt &p)
{
    if (e == enterEvent)
    {
        in_dali_menu = true;
        print_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT, 0);
    }
    else if (e == exitEvent)
    {
        in_dali_menu = false;
        delete_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT);
    }
    return proceed;
}
result set_relay_on()
{
    Serial.println("Setting relay on");
    relay_on_off->set_state(true);
    return proceed;
}

result set_relay_off()
{
    Serial.println("Setting relay off");
    relay_on_off->set_state(false);
    return proceed;
}

int iteration = 1;
bool state_1_10v = false;
bool state_dali = false;
bool state_relay = false;
int current_dali_driver = 0;

result update_dali()
{
    DaliDriver *aux_driver;
    Serial.printf("Update dali to %d driver", current_dali_driver);
    aux_driver = dali_driver_controller->get_by_id(current_dali_driver);
    if (aux_driver != NULL)
    {
        int current_level = aux_driver->current_light_level;
        print_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT, (int)current_level / 254);
    }
    return proceed;
}

// MENU(subMenuAdjustServo, "Adjust Servo Settings", doNothing, noEvent, noStyle
//      // ,FIELD(settingsEEPROM.servoOpen, "Servo Open", " degrees", 0, 180, 10, 1, doNothing, noEvent, noStyle)
//      // ,FIELD(settingsEEPROM.servoClosed, "Servo Closed", " degrees", 0, 180, 10, 1, doNothing, noEvent, noStyle)
//      // ,SUBMENU(subMenu)
//      ,
//      OP("Run!", doFeed, enterEvent), EXIT("<Back"));

// CHOOSE(chooseField, feedDirChoose, "Choose Direction:", doNothing, noEvent, noStyle, VALUE("Forward", 1, doNothing, noEvent), VALUE("Backwards", 0, doNothing, noEvent));

// MENU(subMenuFeedInOut, "Feed Tape", doNothing, noEvent, noStyle, FIELD(feedLength, "Length of Feed:", "mm", 0, 1000, 10, 1, doNothing, noEvent, noStyle), SUBMENU(feedDirChoose), OP("Run!", doFeed, enterEvent), EXIT("<Back"));

// define options for the main menu
TOGGLE(state_1_10v, set_1_10v, "1-10V: ", doNothing, noEvent, wrapStyle,
       VALUE("On", true, set_1_10v_on, enterEvent),
       VALUE("Off", false, set_1_10v_off, enterEvent));

// TOGGLE(state_dali, set_dali, "Dali: ", led_change, enterEvent, wrapStyle,
//        VALUE("On", true, doNothing, noEvent),
//        VALUE("Off", false, doNothing, noEvent));

TOGGLE(state_relay, set_relay, "Relay: ", led_change, enterEvent, wrapStyle,
       VALUE("On", true, set_relay_on, enterEvent),
       VALUE("Off", false, set_relay_off, enterEvent));

//1-10V menu
MENU(menu_1_10V, "1-10V", print_1_10V, anyEvent, wrapStyle,
     SUBMENU(set_1_10v),
     EXIT("< Back"));

MENU(menu_dali, "Dali", print_dali, anyEvent, wrapStyle,
     //SUBMENU(set_dali),
     FIELD(current_dali_driver, "Dali Driver", "", 0, DALI_DRIVERS_NUM - 1, 1, 1, update_dali, updateEvent, noStyle),
     EXIT("< Back"));

MENU(others_menus, "Details", doNothing, noEvent, wrapStyle,
     SUBMENU(menu_1_10V),
     SUBMENU(menu_dali),
     EXIT("< Back"));

// main menu
MENU(mainMenu, "Main", doNothing, noEvent, wrapStyle,
     SUBMENU(set_1_10v),
     //SUBMENU(set_dali),
     SUBMENU(set_relay),
     SUBMENU(others_menus));

// define menu colors --------------------------------------------------------
#define Black RGB565(0, 0, 0)
#define Red RGB565(255, 0, 0)
#define Green RGB565(0, 255, 0)
#define Blue RGB565(0, 0, 255)
#define Gray RGB565(128, 128, 128)
#define LighterRed RGB565(255, 150, 150)
#define LighterGreen RGB565(150, 255, 150)
#define LighterBlue RGB565(150, 150, 255)
#define LighterGray RGB565(211, 211, 211)
#define DarkerRed RGB565(150, 0, 0)
#define DarkerGreen RGB565(0, 150, 0)
#define DarkerBlue RGB565(0, 0, 150)
#define Cyan RGB565(0, 255, 255)
#define Magenta RGB565(255, 0, 255)
#define Yellow RGB565(255, 255, 0)
#define White RGB565(255, 255, 255)
#define DarkerOrange RGB565(255, 140, 0)

// TFT color table
const colorDef<uint16_t> colors[6] MEMMODE = {
    //{{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
    {{(uint16_t)Black, (uint16_t)Black}, {(uint16_t)Black, (uint16_t)Red, (uint16_t)Red}},     //bgColor
    {{(uint16_t)White, (uint16_t)White}, {(uint16_t)White, (uint16_t)White, (uint16_t)White}}, //fgColor
    {{(uint16_t)Red, (uint16_t)Red}, {(uint16_t)Yellow, (uint16_t)Yellow, (uint16_t)Yellow}},  //valColor
    {{(uint16_t)White, (uint16_t)White}, {(uint16_t)White, (uint16_t)White, (uint16_t)White}}, //unitColor
    {{(uint16_t)White, (uint16_t)Gray}, {(uint16_t)Black, (uint16_t)Red, (uint16_t)White}},    //cursorColor
    {{(uint16_t)White, (uint16_t)Yellow}, {(uint16_t)Black, (uint16_t)Red, (uint16_t)Red}},    //titleColor
};

// Define the width and height of the TFT and how much of it to take up
#define GFX_WIDTH 160
#define GFX_HEIGHT 128
#define fontW 12
#define fontH 18

constMEM panel panels[] MEMMODE = {{0, 0, GFX_WIDTH / fontW, GFX_HEIGHT / fontH}}; // Main menu panel
navNode *nodes[sizeof(panels) / sizeof(panel)];                                    //navNodes to store navigation status
panelsList pList(panels, nodes, sizeof(panels) / sizeof(panel));                   //a list of panels and nodes
//idx_t tops[MAX_DEPTH]={0,0}; // store cursor positions for each level
idx_t eSpiTops[MAX_DEPTH] = {0};
TFT_eSPIOut eSpiOut(gfx, colors, eSpiTops, pList, fontW, fontH + 1);
idx_t serialTops[MAX_DEPTH] = {0};
serialOut outSerial(Serial, serialTops);
menuOut *constMEM outputs[] MEMMODE = {&outSerial, &eSpiOut}; //list of output devices
// menuOut *constMEM outputs[] MEMMODE = {&outSerial};            //list of output devices
outputsList out(outputs, sizeof(outputs) / sizeof(menuOut *)); //outputs list
serialIn serial(Serial);
MENU_INPUTS(in, &encStream, &serial); // &encButton,
// MENU_INPUTS(in, &serial); // &encButton,
NAVROOT(nav, mainMenu, MAX_DEPTH, in, out);

void screen_task(void *pvParameters)
{
    Serial.println("Task running");
    BaseType_t xStatus;
    ScreenMessage msg;
    // screen->tft->fillRoundRect(1, 1, 158, 16, 4, TFT_ORANGE);
    // screen->tft->setTextColor(TFT_WHITE, TFT_ORANGE);

    // main menu
    // screen->tft->drawString("Main", MENU_X, MENU_Y, MENU_FONT);
    // screen->draw_triangle(10, 2, 3, true, true);
    // uint32_t pos_x = screen->tft->textWidth("Main", 2);
    // screen->draw_triangle(10, MENU_X + pos_x + 2 + 10, 3, false, false);

    for (;;)
    {
        xStatus = xQueueReceive(screen_queue, &msg, portMAX_DELAY);

        nav.poll();
        iteration++;

        // Serial.printf("Variable: %d\n", iteration);

        if (xStatus != pdPASS)
        {
            continue;
        }

        //Serial.println("Message received!");
        switch (msg.msg_type)
        {
        case TestMsg: // used for debug developments
            // screen->tft->drawCentreString("Test #" + String(msg.value_i32), 30, 0, 4); //abs(last_count % 8));
            break;
        case Value1_10vMsg: // print 1-10v
            // screen->tft->drawCentreString("1-10V: " + String(msg.value_float), 30, 30, 2); //abs(last_count % 8));
            // Serial.printf("1-10V: %f\n", msg.value_float);
            if (in_1_10v_menu)
            {
                float percentage = min(100.0, msg.value_float / 100.0);
                print_text(STRING_POSX_3, STRING_POSY_3, String("Raw : "), String(msg.value_float / 1000.0) + "V   "); //do not remove the end spaces, to clear previous larger strings
                print_text(STRING_POSX_2, STRING_POSY_2, String("Perc: "), String(percentage, 2) + "%    ");
                print_percentage_bar(RECT_POSX, RECT_POSY, RECT_WIDTH, RECT_HEIGHT, percentage);
                // Serial.println("In 1-10v");
            }
            break;
        case TimeMsg:
            gfx.setTextColor(TFT_WHITE, TFT_ORANGE);                // Adding a black background colour erases previous text automatically
            gfx.drawString(msg.message, TIME_X, TIME_Y, TIME_FONT); //abs(last_count % 8));
            break;
        case EncoderMsg:
            Serial.printf("Encoder message received: count %d", msg.value_i32);
            break;
        default:
            Serial.println("Unknow message type");
            break;
        }
    }
}

void update_screen()
{
    // update state variables
    state_1_10v = driver_1_10v->get_state();
    state_relay = relay_on_off->get_state();
    Serial.printf("Updating screen: %d\n", state_1_10v);
    nav.refresh();
}

void setup_screen()
{
    gfx.init();
    gfx.setRotation(2);
    gfx.fillScreen(TFT_BLACK);
    //gfx.setTextColor(TFT_GREEN, TFT_LIGHTGREY); // Adding a black background colour erases previous text automatically
    gfx.setTextFont(2); // font size change the font. Carefull with font size definition.

    // create queue to receive data
    screen_queue = xQueueCreate(MAX_RECEIVE_SCREEN_QUEUE, sizeof(ScreenMessage));
}
// Screen::Screen()
// {

//     tft = new TFT_eSPI(); // Invoke library, pins defined in User_Setup.h

//     tft->init();
//     tft->setRotation(1);
//     tft->fillScreen(TFT_LIGHTGREY);
//     tft->setTextColor(TFT_GREEN, TFT_LIGHTGREY); // Adding a black background colour erases previous text automatically

//     // create queue to receive data
//     screen_queue = xQueueCreate(MAX_RECEIVE_SCREEN_QUEUE, sizeof(ScreenMessage));
// };

// void Screen::draw_triangle(uint32_t height, uint32_t pos_x, uint32_t pos_y, bool left, bool selected)
// {
//     uint32_t pos_x0 = pos_x;
//     uint32_t pos_y0 = left ? pos_y + height / 2 : pos_y;
//     uint32_t pos_x1 = left ? pos_x + height : pos_x;
//     uint32_t pos_y1 = left ? pos_y : pos_y + height;
//     uint32_t pos_x2 = pos_x + height;
//     uint32_t pos_y2 = left ? pos_y + height : pos_y + height / 2;

//     if (selected)
//     {
//         tft->fillRoundRect(pos_x - 1, pos_y - 1, height + 4, height + 4, 1, TFT_WHITE);
//     }

//     tft->fillTriangle(pos_x0, pos_y0, pos_x1, pos_y1, pos_x2, pos_y2, selected ? TFT_BLACK : TFT_WHITE);
// }
