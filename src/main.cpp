#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <Arduino.h>
#include "main.h"
// include to have logs
// #include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

// to get time from a NTP server
#include "time.h"

#include "my_pin_number.h"

#include <ArduinoJson.h>
// include driver for the screen
//  #include "driver_screen.h"

// include driver for sd
#include "driver_sd.h"

// include server related functions and tasks
#include "driver_server.h"

// dali uart tasks
#include "driver_dali_uart.h"

// M5 Stack
#ifdef BOARD_M5STACK
#include <M5Stack.h>
#endif

#ifndef BOARD_M5STACK
const int LED_PIN = LED_BUILTIN;

#else
const int LED_PIN = 0;
#endif

// logging tag
static const char *TAG = "Main";

// define relay pin(s)
#define RELAY_ON_OFF_DEFAULT_STATE true

#ifdef USE_1_10V
// create 1_10v driver
Driver_1_10v *driver_1_10v;
#endif

#ifdef USE_LIGHT_SENSOR
// light sensor
LightSensor *light_sensor;
#endif

#ifdef USE_RELAY
// create realy global  object
Relay *relay_on_off;
#endif

#ifdef USE_DIGITAL
// create DIN object
DIN *digital_in;
#endif

#ifdef USE_LED
// create LED object
Driver_LED *photocell_led;
#endif

#ifdef USE_NVS
// create an object for the spi flash system
MySPIFFS *my_spifss;

// create an Non-volatile storage object
MyNVS *my_nvs;
#endif

#ifdef USE_SD
// create SD card object
MySD *my_sd;
#endif

#ifdef USE_DALI_UART
DaliUart *my_dali_uart;
#endif

void print_task_stats(TaskHandle_t handle)
{

  char *task_list_str;
  task_list_str = pcTaskGetTaskName(handle);
  Serial.printf("Task name: %s\n", task_list_str);

  // total number of tasks
  int total_number_tasks = uxTaskGetNumberOfTasks();
  Serial.printf("Number of tasks: %d\n", total_number_tasks);

  // get priority
  int priority = uxTaskPriorityGet(handle);
  Serial.printf("Task priority: %d\n", priority);

  // high mark
  int highmark = uxTaskGetStackHighWaterMark(handle);
  Serial.printf("High mark: %d\n", highmark);

  int core = xPortGetCoreID();
  Serial.printf("Current Core (not task core): %d\n", core);
}
// helper function to print stats
void print_current_task_stats(void)
{
  TaskHandle_t handle = xTaskGetCurrentTaskHandle();
  print_task_stats(handle);
}

// void print_task_stats_name(const char *task_name)
// {
//   TaskHandle_t handle = xTaskGetHandle(task_name);
//   print_task_stats(handle);
// }

// create screen object
// create queue that will be used to communicate between
#ifdef USE_SCREEN

ScreenMessage msg;
QueueHandle_t screen_queue;

// inform screen of change
void inform_screen_on_change_bool(ScreenMessageType msg_type, bool state)
{
  ScreenMessage msg;
  msg.msg_type = msg_type;
  msg.value_bool = state;
  xQueueSend(screen_queue, &msg, 0);
}
#endif // USE_SCREEN end

#ifdef USE_SERIAL
// global buffer for serial port
int serial_bytes_read = 0;
const int SERIAL_BUFFER_SIZE = 1024;
char serial_buffer[SERIAL_BUFFER_SIZE];

// send timer for serial port
void vSendTimerCallback(TimerHandle_t xTimer)
{
  // Serial.printf("Timer called");
  serial_buffer[serial_bytes_read] = '\0';

  // send bytes
  ws.textAll(serial_buffer, serial_bytes_read);
  serial_bytes_read = 0;
}

// RTOS task to read from the serial port and send data to websocket
void serial_port_reader_task(void *parameter)
{

  int bytes_available = 0;
  int bytes_read_single = 0;
  int buffer_remaining_size = SERIAL_BUFFER_SIZE;

  int tick_delay = 100 / portTICK_PERIOD_MS;

  Serial.printf("Tick delay: %d\n", tick_delay);
  // Create a timer
  TimerHandle_t send_timer = xTimerCreate("send_timer",
                                          tick_delay,
                                          pdFALSE,
                                          (void *)0,
                                          vSendTimerCallback);

  if (send_timer == NULL)
  {
    Serial.println("Problem creating timer");
  }

  Serial.println("Starting serial port reader task");
  while (true)
  {
    bytes_available = Serial2.available();

    while (bytes_available)
    {
#ifdef DEBUG_CONSOLE
      Serial.println("Bytes available: " + String(bytes_available));
#endif
      buffer_remaining_size = SERIAL_BUFFER_SIZE - serial_bytes_read;
      bytes_read_single = Serial2.readBytes(&serial_buffer[serial_bytes_read], bytes_available < buffer_remaining_size ? bytes_available : buffer_remaining_size - 1);
      serial_bytes_read += bytes_read_single;

#ifdef DEBUG_CONSOLE
      Serial.println("Read data: " + String(buffer));
#endif
      if (bytes_read_single > 0)
      {
#ifdef DEBUG_CONSOLE
        Serial.println("Bytes read: " + String(bytes_read));
#endif
        // start timer if not already started
        if (pdPASS != xTimerStart(send_timer, 0))
        {
          Serial.println("Could not start timer!");
        }
#ifdef DEBUG_CONSOLE
        Serial.println("Sending completed!");
#endif
      }

      bytes_available = Serial2.available();
    }
    delay(10); // some delay between readings
  }
}

#endif

void setup()
{
// turn off the speaker
#ifdef BOARD_M5STACK
  M5.begin();

  // set speaker mute
  M5.Speaker.begin();
#endif // BOARD_M5STACK

  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);

  // begin serial port
  Serial.begin(115200);

#ifdef USE_SCREEN
  // creation a screen interface
  Serial.println("Creating a Screen interface");

  // screen = new Screen();
  setup_screen();

  // create task for screen
  // start receive task
  xTaskCreatePinnedToCore(
      screen_task,          /* Task function. */
      "ScreenTask",         /* String with name of task. */
      SCREEN_TASK_STACK,    /* Stack size in bytes. */
      NULL,                 /* Parameter passed as input of the task */
      SCREEN_TASK_PRIORITY, /* Priority of the task. */
      NULL,                 /* Task handle. */
      SCREEN_TASK_CORE_ID); /* Core where screen task runs */

#endif // USE_SCREEN

#ifdef USE_SERIAL
  // begin second serial port
  Serial2.begin(115200, SERIAL_8N1, EXTERNAL_RXD2, EXTERNAL_TXD2);
  Serial2.setTimeout(1000); // set timeout to 1 second

  // logging
  Serial.println("Serial Txd is on pin: " + String(TX));
  Serial.println("Serial Rxd is on pin: " + String(RX));
#endif

  esp_log_level_set("*", ESP_LOG_VERBOSE);
  esp_log_level_set("DALI", ESP_LOG_INFO);
  esp_log_level_set("DALI_COM", ESP_LOG_INFO);

  // start of logging
  ESP_LOGI(TAG, "Initializing setup");

#ifdef USE_1_10V
  // Create 1_10v driver
  driver_1_10v = new Driver_1_10v();
#endif

#ifdef USE_LIGHT_SENSOR
  // Create LightSensor
  light_sensor = new LightSensor();
#endif

#ifdef USE_SERIAL
  TaskHandle_t xHandle = NULL;

  // start serial receive/transmit task
  xTaskCreate(
      serial_port_reader_task, /* Task function. */
      "SerialReceiveTask",     /* String with name of task. */
      SERIAL_TASK_STACK,       /* Stack size in bytes. */
      NULL,                    /* Parameter passed as input of the task */
      SERIAL_TASK_PRIORITY,    /* Priority of the task. */
      &xHandle);               /* Task handle. */

#endif

#ifdef USE_NVS
  // create NVS object
  my_nvs = new MyNVS();

  uint8_t bootcount = my_nvs->get_uint8("bootcount", 0);
  my_nvs->set_uint8("bootcount", ++bootcount);
  Serial.printf("Bootcount: %d\n", bootcount);

#endif

#ifdef USE_DALI

  // create Dali communications
  dali_comm = new DALI_COMM(DALI_RX_PIN, DALI_TX_PIN, DALI_COMM_TX_PRIORITY, DALI_COMM_RX_PRIORITY, DALI_TX_IDLE_POLARITY,
                            DALI_COMM_TX_STACK, DALI_COMM_RX_STACK);

#if (DALI_DRIVERS_NUM > 0)
  {
    // create dali controller
    dali_driver_controller = new DaliDriverController();

    // Number of drivers defined in main.h
    for (int i = 0; i < DALI_DRIVERS_NUM; ++i)
    {
      // create dali driver i
      dali_driver_controller->create_dali_driver(i, CREATE_D4i);
    }
  }
#endif // end of DALI_DRIVERS_NUM

#ifdef USE_DALI_UART
  my_dali_uart = new DaliUart();

  dali_comm->register_queue(&dali_uart_receive_queue);

  TaskHandle_t xHandle = NULL;

  // start serial receive/transmit task
  xTaskCreate(
      data_uart_task,            /* Task function. */
      "DaliUartTask",            /* String with name of task. */
      DALI_UART_TASK_STACK,      /* Stack size in bytes. */
      NULL,                      /* Parameter passed as input of the task */
      DALI_SERIAL_TASK_PRIORITY, /* Priority of the task. */
      &xHandle);                 /* Task handle. */

#endif // USE_DALI_UART

#endif // end of USE_DALI

#ifdef USE_RELAY
  Serial.println("Setup relays!");
  relay_on_off = new Relay(RELAY_ON_OFF_PINNUMBER, RELAY_ON_OFF_DEFAULT_STATE);
#endif

#ifdef USE_DIGITAL
  // Digital input
  Serial.println("Setup digital input!");
  digital_in = new DIN(DIN_READ_PINNUMBER, DIN_SET_PINNUMBER);
#endif

#ifdef USE_LED
  // creating a led interface
  Serial.println("Creating LED interface");
  photocell_led = new Driver_LED(LED_PINNUMBER);
#endif

#ifdef USE_SD
  // Create an SD card interface
  Serial.println("Creating a SD card interface");
  my_sd = new MySD();
#endif

#ifdef USE_NVS
  // Create an SPI falsh interface
  Serial.println("Creating an SPI flash interface");
  my_spifss = new MySPIFFS();
#endif

#ifdef USE_SERVER
  // create server task
  TaskHandle_t server_handle = NULL;

  // start serial receive/transmit task
  xTaskCreate(
      server_task,          /* Task function. */
      "ServerTask",         /* String with name of task. */
      SERVER_TASK_STACK,    /* Stack size in bytes. */
      NULL,                 /* Parameter passed as input of the task */
      SERVER_TASK_PRIORITY, /* Priority of the task. */
      &server_handle);      /* Task handle. */

  Serial.printf("Server priority: %d\n", uxTaskPriorityGet(server_handle));
#endif

  ESP_LOGI(TAG, "Setup finished!");
}

long last_message_millis = 0;
int startup_time = millis();
uint32_t i = 0;
bool relay_status = false;
String output;
StaticJsonDocument<200> doc;

// this is a free rtos task
void loop()
{
  if (millis() - last_message_millis > 1000)
  {

#ifdef USE_1_10V
    // inform of 1-10V connection
    doc.clear();
    float voltage_1_10V = driver_1_10v->read_1_10v();
    output = "";

    doc["type"] = "1_10V";
    doc["volt_1_10V"] = voltage_1_10V;
    doc["votl_1_10V_perc"] = voltage_1_10V * 10.0;

    // ws_dali.textAll(dali_frame_to_string(frame));

    serializeJsonPretty(doc, output);

    // Serial.println(output);
    ws_dali.textAll(output);

#ifdef USE_SCREEN
    msg.msg_type = Value1_10vMsg;
    msg.value_float = driver_1_10v->read_1_10v();

    xQueueSend(screen_queue, &msg, 0);

#endif // USE_SCREEN

#endif // USE_1_10V

#ifdef USE_LIGHT_SENSOR
    // inform of lux reading
    // Serial.println("Updating light sensor");
    doc.clear();
    long lux = light_sensor->read_lux();
    output = "";

    doc["type"] = "Light";
    doc["lux"] = lux;

    // ws_dali.textAll(dali_frame_to_string(frame));

    serializeJsonPretty(doc, output);

    // Serial.println(output);
    ws_dali.textAll(output);

#ifdef USE_SCREEN
    msg.msg_type = LightMsg;
    msg.value_float = (float)lux;

    xQueueSend(screen_queue, &msg, 0);

#endif // USE_SCREEN

#endif // USE_LIGHT_SENSOR

// send test message for the dali
#ifdef SIMULATE_DALI_LIGHT_LEVELS
    inform_light_level_change(0, random(1, 254));
    inform_light_level_change(1, random(1, 254));
#endif

    last_message_millis = millis();
    i++;
  }
  delay(100); // wait a bit between acalls
}