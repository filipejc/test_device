#ifdef USE_SCREEN

#include "driver_screen.h"
#include "driver_dali.h"

#include "main.h"
#include <Arduino.h>

#if defined(TFT_ADAFRUIT_2_4) || defined(BOARD_M5STACK)
#include <lvgl.h>

#ifdef BOARD_M5STACK
#include <TFT_eSPI.h>
#else // 2.4 Adafruit_ILI9341

#include <Adafruit_LvGL_Glue.h>
#include <Adafruit_STMPE610.h>
#include <Adafruit_ILI9341.h>
#endif

// external logo
#ifdef LV_EXTEND_NAMES
extern const lv_img_dsc_t logo;
#else
extern const lv_img_dsc_t logo;
#endif

/**********************
 *  STATIC VARIABLES
 **********************/

static lv_group_t *g; // Group for focus
static lv_obj_t *tv;
static lv_obj_t *t1;
static lv_obj_t *t2;
static lv_obj_t *t3;
static lv_obj_t *t4; // about

// static lv_style_t style_box;

// controls

static lv_obj_t *lsi_led;               // display status in a led
static lv_obj_t *sw_1_10v;              // switch for 1-10V
static lv_obj_t *bar_1_10v;             // bar to 1-10v
static lv_obj_t *dali_light_status_led; // status of the light
static lv_obj_t *driver_list;           // list to choose the driver number

static lv_obj_t *label_1_10v; // display the 1-10V value
static lv_obj_t *label_lux;   // display light level

// static lv_obj_t *label_1_10v_perc;  // display the 1-10V in percentage

const int16_t BAR_1_10V_MAX = 11000; // define maximum value for 1-10V
const int16_t MAX_1_10V = 10000;     // maximum value
const int16_t MAX_BUFFER_MEAS = 30;  // maximum buffer size

struct
{
    lv_obj_t *sw_relay;      // to control relay
    lv_obj_t *sw_lsi;        // switch for the lsi
    lv_obj_t *sw_1_10v_main; // switch for 1-10V in main tab
} main_tab_obj;

struct
{

} tab_1_10v_obj;

#ifdef USE_BUTTONS_ENCODER
// variables for encoder
static int16_t enc_diff = 0;
static lv_indev_state_t state = LV_INDEV_STATE_REL;

bool encoder_with_keys_read(lv_indev_drv_t *drv, lv_indev_data_t *data)
{
    data->state = state;
    data->enc_diff = enc_diff;
    enc_diff = 0;

    return false; /*No buffering now so no more data read*/
}

static void focus_cb(lv_group_t *group)
{
    lv_obj_t *obj = lv_group_get_focused(group);
    Serial.println("focus callback!");

    if (obj != tv)
    {
        uint16_t tab = lv_tabview_get_tab_act(tv);
        switch (tab)
        {
        case 0:
            lv_page_focus(t1, obj, LV_ANIM_ON);
            break;
        case 1:
            lv_page_focus(t2, obj, LV_ANIM_ON);
            break;
#ifdef USE_DALI
        case 2:
            lv_page_focus(t3, obj, LV_ANIM_ON);
            break;
#endif
        }
    }
}
#endif // USE_BUTTONS_ENCODER

static void tv_event_cb(lv_obj_t *ta, lv_event_t e)
{
    if (e == LV_EVENT_VALUE_CHANGED || e == LV_EVENT_REFRESH)
    {
        Serial.printf("Tab event!");

        lv_group_remove_all_objs(g);

        uint16_t tab = lv_tabview_get_tab_act(tv);
        size_t size = 0;
        lv_obj_t **objs = NULL;
        if (tab == 0)
        {
            size = sizeof(main_tab_obj);
            objs = (lv_obj_t **)&main_tab_obj;
        }
        else if (tab == 1)
        {
            size = sizeof(tab_1_10v_obj);
            objs = (lv_obj_t **)&tab_1_10v_obj;
        }

        lv_group_add_obj(g, tv);

        uint32_t i;
        for (i = 0; i < size / sizeof(lv_obj_t *); i++)
        {
            if (objs[i] == NULL)
                continue;
            lv_group_add_obj(g, objs[i]);
        }
    }
}

// Screen objects
#ifndef BOARD_M5STACK
Adafruit_ILI9341 tft(TFT_CS, TFT_DC);
Adafruit_LvGL_Glue glue;
#else

TFT_eSPI tft = TFT_eSPI(); /* TFT instance */
static lv_disp_buf_t disp_buf;
static lv_color_t buf[LV_HOR_RES_MAX * 10];

#endif

#ifdef USE_TOUCHSCREEN
Adafruit_STMPE610 ts(STMPE_CS);
#endif

char dali_driver_num[32] = "1\n";

void set_1_10v_value(float value_mv)
{
    // Serial.println("set");
    char buffer[MAX_BUFFER_MEAS];
    snprintf(buffer, MAX_BUFFER_MEAS, "Value: %.1fV,   %.f%%", value_mv / 1000.0, (float)value_mv / MAX_1_10V * 100.0); // make the string
    lv_label_set_text(label_1_10v, buffer);
    // set value in bar
    lv_bar_set_value(bar_1_10v, (int16_t)value_mv, LV_ANIM_ON);
}

void set_lux_value(float lux)
{
    // Serial.println("set");
    char buffer[MAX_BUFFER_MEAS];
    snprintf(buffer, MAX_BUFFER_MEAS, "Light: %l lux", (long)lux); // make the string
    lv_label_set_text(label_lux, buffer);
}

void sw_relay_event_cb(lv_obj_t *sw, lv_event_t event)
{
    // static last_change_time = millis();
    // if (event == LV_EVENT_CLICKED)
    if (event == LV_EVENT_VALUE_CHANGED)
    {
#ifdef LV_EXTEND_NAMES
        bool state = lv_switch_get_state(sw);
#else
        bool state = lv_sw_get_state(sw);
#endif
        Serial.printf("SW for relay state changed: %d\n", state);
        relay_on_off->set_state(state);
    }
}

void sw_lsi_event_cb(lv_obj_t *sw, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
#ifdef LV_EXTEND_NAMES
        bool state = lv_switch_get_state(sw);
#else
        bool state = lv_sw_get_state(sw);
#endif
        Serial.printf("SW for LSI changed: %d\n", state);
        digital_in->set_state(state);
    }
}

void update_screen_1_10v(bool state)
{
    // 1-10V
    if (state)
    {
#ifdef LV_EXTEND_NAMES
        lv_switch_on(sw_1_10v, LV_ANIM_ON);
        lv_switch_on(main_tab_obj.sw_1_10v_main, LV_ANIM_ON);
#else
        lv_sw_on(sw_1_10v, LV_ANIM_ON);
        lv_sw_on(main_tab_obj.sw_1_10v_main, LV_ANIM_ON);
#endif
    }
    else
    {

#ifdef LV_EXTEND_NAMES
        lv_switch_off(sw_1_10v, LV_ANIM_ON);
        lv_switch_off(main_tab_obj.sw_1_10v_main, LV_ANIM_ON);
#else

        lv_sw_off(sw_1_10v, LV_ANIM_ON);
        lv_sw_off(main_tab_obj.sw_1_10v_main, LV_ANIM_ON);
#endif
    }
}

#ifdef USE_USE_1_10V_GEN
void sw_1_10v_event_cb(lv_obj_t *sw, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
#ifdef LV_EXTEND_NAMES
        bool state = lv_switch_get_state(sw);
#else
        bool state = lv_sw_get_state(sw);
#endif
        Serial.printf("SW for 1-10V changed: %d\n", state);
        driver_1_10v->set_state(state);

        // to keep screen updated between different menus
        update_screen_1_10v(state);
    }
}
#endif

#ifdef USE_DALI
static void dali_driver_sel_cb(lv_obj_t *obj, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
#ifdef LV_EXTEND_NAMES
        lv_dropdown_get_selected_str(obj, dali_driver_num, sizeof(dali_driver_num));
#else
        lv_ddlist_get_selected_str(obj, dali_driver_num, sizeof(dali_driver_num));
#endif
        printf("Option: %s\n", dali_driver_num);
    }
}

static void simulate_fault_event_cb(lv_obj_t *btn, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
#ifdef LV_EXTEND_NAMES
        bool state = lv_switch_get_state(btn);
#else

        bool state = lv_sw_get_state(btn);
#endif

        int driver_num = atoi(dali_driver_num) - 1;

        Serial.printf("Driver number: %s, converted to int: %d\n", dali_driver_num, driver_num);

        dali_driver_controller->get_by_id(driver_num)->set_simulate_failure(state);
        Serial.printf("SW for simulate event state changed: %d\n", state);
    }
}

#endif // USE_DALI

lv_obj_t *create_horz_cont(lv_obj_t *parent)
{
    lv_obj_t *sm_cont = lv_cont_create(parent, NULL);
    lv_obj_set_drag_parent(sm_cont, true);

#ifdef LV_EXTEND_NAMES
    lv_cont_set_layout(sm_cont, LV_LAYOUT_ROW_MID);
#else
    lv_cont_set_layout(sm_cont, LV_LAYOUT_ROW_M);
#endif
    lv_cont_set_fit(sm_cont, LV_FIT_TIGHT);
    return sm_cont;
}

static void main_tab_create(lv_obj_t *parent)
{
#ifdef LV_EXTEND_NAMES
    lv_page_set_scrl_layout(parent, LV_LAYOUT_COLUMN_MID);
#else
    lv_page_set_scrl_layout(parent, LV_LAYOUT_PRETTY);
#endif

    // create small container for the label and switch
    lv_obj_t *sm_cont = create_horz_cont(parent);

    // relay label
    lv_obj_t *label = lv_label_create(sm_cont, NULL);
    lv_label_set_text(label, "Relay:");

    // relay switch

#ifdef LV_EXTEND_NAMES
    main_tab_obj.sw_relay = lv_switch_create(sm_cont, NULL);
#else
    main_tab_obj.sw_relay = lv_sw_create(sm_cont, NULL);
#endif

    // add calback for relay switch
    lv_obj_set_event_cb(main_tab_obj.sw_relay, sw_relay_event_cb);

#ifdef USE_USE_1_10V_GEN
    // 1-10V label
    // create small container
    sm_cont = create_horz_cont(parent);
    lv_obj_t *label_1_10v = lv_label_create(sm_cont, NULL);
    lv_label_set_text(label_1_10v, "1-10V:");

    // 1-10V switch
#ifdef LV_EXTEND_NAMES
    sw_1_10v_main = lv_switch_create(sm_cont, NULL);
#else
    main_tab_obj.sw_1_10v_main = lv_sw_create(sm_cont, NULL);
#endif
    lv_obj_set_event_cb(main_tab_obj.sw_1_10v_main, sw_1_10v_event_cb);
#endif

    // LSI
    // create small container
    sm_cont = create_horz_cont(parent);
    // label

    lv_obj_t *label_lsi = lv_label_create(sm_cont, NULL);
    lv_label_set_text(label_lsi, "LSI:");
    // led to display status
    lsi_led = lv_led_create(sm_cont, NULL);
    // switch to control the status

#ifdef LV_EXTEND_NAMES
    main_tab_obj.sw_lsi = lv_switch_create(sm_cont, NULL);
#else
    main_tab_obj.sw_lsi = lv_sw_create(sm_cont, NULL);
#endif

    lv_obj_set_event_cb(main_tab_obj.sw_lsi, sw_lsi_event_cb);
}

static void tab_create_1_10v(lv_obj_t *parent)
{
    lv_obj_t *sm_cont;
#ifdef LV_EXTEND_NAMES
    lv_page_set_scrl_layout(parent, LV_LAYOUT_COLUMN_MID);
#else
    lv_page_set_scrl_layout(parent, LV_LAYOUT_COL_M);
#endif

#ifdef USE_USE_1_10V_GEN
    // 1-10V switch
    sm_cont = create_horz_cont(parent);
    label_1_10v = lv_label_create(sm_cont, NULL);
    lv_label_set_text(label_1_10v, "1-10V:");

#ifdef LV_EXTEND_NAMES
    sw_1_10v = lv_switch_create(sm_cont, NULL);
#else

    sw_1_10v = lv_sw_create(sm_cont, NULL);
#endif
    lv_obj_set_event_cb(sw_1_10v, sw_1_10v_event_cb);

#endif
    //
    // create small container
    sm_cont = create_horz_cont(parent);

    label_1_10v = lv_label_create(sm_cont, NULL);

    bar_1_10v = lv_bar_create(parent, NULL);
    lv_bar_set_anim_time(bar_1_10v, 0);
    lv_bar_set_range(bar_1_10v, 0, BAR_1_10V_MAX);

#ifdef USE_LIGHT_SENSOR

    sm_cont = create_horz_cont(parent);

    label_lux = lv_label_create(sm_cont, NULL);
    lv_label_set_text(label_lux, "Lux: NA");

#endif // USE_LIGHT_SENSOR
}

// on main lvgl, functions have "dropdown" in the name,
// in arduino, the functions have "ddlist"

#ifdef USE_DALI
// Create tab for DALI
static void tab_create_dali(lv_obj_t *parent)
{
#ifdef LV_EXTEND_NAMES
    lv_page_set_scrl_layout(parent, LV_LAYOUT_COLUMN_MID);
#else
    lv_page_set_scrl_layout(parent, LV_LAYOUT_PRETTY);
#endif
    // // add calback
    lv_obj_t *sm_cont = create_horz_cont(parent);
    lv_obj_t *label_dali = lv_label_create(sm_cont, NULL);
    // Choose dali Driver
    lv_label_set_text(label_dali, "Choose Dali Driver:");
#ifdef LV_EXTEND_NAMES
    driver_list = lv_dropdown_create(sm_cont, NULL);
    lv_dropdown_set_options(driver_list, "1\n"
                                         "2");
#else
    driver_list = lv_ddlist_create(sm_cont, NULL);
    lv_ddlist_set_options(driver_list, "1\n"
                                       "2");
#endif
    lv_obj_set_event_cb(driver_list, dali_driver_sel_cb);

    // create small container
    lv_obj_t *small_container = create_horz_cont(parent);
    dali_light_status_led = lv_led_create(small_container, NULL);

    lv_obj_t *label_dali_value = lv_label_create(small_container, NULL);
    lv_label_set_text(label_dali_value, "Light: ");
    lv_obj_t *bar1 = lv_bar_create(small_container, NULL);
    lv_bar_set_anim_time(bar1, 2000);
    lv_bar_set_value(bar1, 100, LV_ANIM_ON);
    lv_obj_set_width(bar1, 100);

    // create small container
    small_container = create_horz_cont(parent);

    lv_obj_t *label_simulate_fault = lv_label_create(small_container, NULL);
    lv_label_set_text(label_simulate_fault, "Simulate fault: ");
#ifdef LV_EXTEND_NAMES
    lv_obj_t *sw = lv_switch_create(small_container, NULL);
#else
    lv_obj_t *sw = lv_sw_create(small_container, NULL);
#endif

    // add calback
    lv_obj_set_event_cb(sw, simulate_fault_event_cb);
}
#endif // USE_DALI

// create a tab about
static void tab_create_about(lv_obj_t *parent)
{
#ifdef LV_EXTEND_NAMES
    lv_page_set_scrl_layout(parent, LV_LAYOUT_COLUMN_MID);
#else
    lv_page_set_scrl_layout(parent, LV_LAYOUT_PRETTY);
#endif
    // // add calback
    lv_obj_t *sm_cont = create_horz_cont(parent);
    lv_obj_t *label_about = lv_label_create(sm_cont, NULL);

    lv_label_set_text(label_about, "Developed by: Filipe Coelho\nDate: 30-06-2020\nContact: fcoelho@schreder.com");

    sm_cont = create_horz_cont(parent);

    lv_obj_t *img = lv_img_create(sm_cont, NULL);

    lv_img_set_src(img, &logo);
}

// update screen relay
void update_screen_relay(bool state)
{
    // relay on off
    if (state)
    {
#ifdef LV_EXTEND_NAMES
        lv_switch_on(main_tab_obj.sw_relay, LV_ANIM_ON);
#else
        lv_sw_on(main_tab_obj.sw_relay, LV_ANIM_ON);
#endif
    }
    else
    {
#ifdef LV_EXTEND_NAMES
        lv_switch_off(main_tab_obj.sw_relay, LV_ANIM_ON);
#else
        lv_sw_off(main_tab_obj.sw_relay, LV_ANIM_ON);
#endif
    }
}

// update screen with LSI set
void update_screen_lsi_set(bool state)
{
    if (state)
    {
#ifdef LV_EXTEND_NAMES
        lv_switch_on(main_tab_obj.sw_lsi, LV_ANIM_ON);
#else
        lv_sw_on(main_tab_obj.sw_lsi, LV_ANIM_ON);
#endif
    }
    else
    {
#ifdef LV_EXTEND_NAMES
        lv_switch_off(main_tab_obj.sw_lsi, LV_ANIM_ON);
#else
        lv_sw_off(main_tab_obj.sw_lsi, LV_ANIM_ON);
#endif
    }
}

// update screen with LSI get
void update_screen_lsi_get(bool state)
{
    if (state)
    {
        lv_led_on(lsi_led);
    }
    else
    {
        lv_led_off(lsi_led);
    }
}

#ifdef BOARD_M5STACK
/* Display flushing */
void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
    uint32_t w = (area->x2 - area->x1 + 1);
    uint32_t h = (area->y2 - area->y1 + 1);

    tft.startWrite();
    tft.setAddrWindow(area->x1, area->y1, w, h);
    tft.pushColors(&color_p->full, w * h, true);
    tft.endWrite();

    lv_disp_flush_ready(disp);
}
#endif

#ifdef USE_BUTTONS_ENCODER

void IRAM_ATTR button_a_func()
{
    enc_diff -= 1;
    Serial.printf("Button A pressed: %d\n", enc_diff);
}

void IRAM_ATTR button_b_func()
{
    state = digitalRead(BUTTON_B_PIN) ? LV_BTN_STATE_REL : LV_BTN_STATE_PR;
    Serial.printf("Button B pressed: %d\n", state);
}

void IRAM_ATTR button_c_func()
{
    enc_diff += 1;
    Serial.printf("Button C pressed: %d\n", enc_diff);
}

#endif // USE_BUTTONS_ENCODER

void setup_screen()
{

    Serial.println("Start screen setup");

    // initialize
    lv_init();
    // Initialize display and touchscreen BEFORE glue setup
    tft.begin();
    tft.setRotation(TFT_ROTATION);

    // litle delay for the touchscreen to start
    delay(100);

#ifdef BOARD_M5STACK
    lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);

    /*Initialize the display*/
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.hor_res = 320;
    disp_drv.ver_res = 240;
    disp_drv.flush_cb = my_disp_flush;
    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);
#else

    /*Initialize the display*/
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.hor_res = 320;
    disp_drv.ver_res = 240;

#endif

#ifdef USE_BUTTONS_ENCODER
    /*Initialize the input device driver*/
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_ENCODER;
    indev_drv.read_cb = encoder_with_keys_read;
    lv_indev_t *enc_indev = lv_indev_drv_register(&indev_drv);

    // create screen

    g = lv_group_create();
    lv_group_set_focus_cb(g, focus_cb);
    lv_indev_set_group(enc_indev, g);

#endif // USE_BUTTONS_ENCODER

#ifdef USE_TOUCHSCREEN
    if (!ts.begin())
    {

        Serial.println("Couldn't start touchscreen controller");

        for (;;)
            ;
    }

    // Serial.println("test0");
    // Initialize glue, passing in address of display & touchscreen
    LvGLStatus status = glue.begin(&tft, &ts);
    if (status != LVGL_OK)
    {
        Serial.printf("Glue error %d\r\n", (int)status);
        for (;;)
            ;
    }
#endif

#ifdef USE_BUTTONS_ENCODER
    // configure buttons
    // setup buttons
    pinMode(BUTTON_A_PIN, INPUT);
    pinMode(BUTTON_B_PIN, INPUT);
    pinMode(BUTTON_C_PIN, INPUT);

    digitalWrite(BUTTON_A_PIN, HIGH);
    digitalWrite(BUTTON_B_PIN, HIGH);
    digitalWrite(BUTTON_B_PIN, HIGH);

    //attachInterrupt(BUTTON_A_PIN, button_a_func, FALLING);
    attachInterrupt(BUTTON_B_PIN, button_b_func, FALLING);
    attachInterrupt(BUTTON_C_PIN, button_c_func, FALLING);

#endif

#if LV_EXTEND_NAMES
    static lv_theme_t *lv_theme = lv_theme_material_init(LV_COLOR_OLIVE, LV_THEME_DEFAULT_COLOR_SECONDARY, LV_THEME_MATERIAL_FLAG_LIGHT,
                                                         LV_THEME_DEFAULT_FONT_SMALL, LV_THEME_DEFAULT_FONT_NORMAL, LV_THEME_DEFAULT_FONT_SUBTITLE, LV_THEME_DEFAULT_FONT_TITLE);
    lv_theme_set_act(lv_theme);
#else
    //static lv_theme_t *lv_theme = lv_theme_material_init(210, NULL);
    //lv_theme_set_current(lv_theme);
#endif
    // create tabs
    lv_obj_t *scr = lv_scr_act();
    Serial.println("after scr");
    tv = lv_tabview_create(scr, NULL);
    //lv_obj_set_event_cb(tv, tv_event_cb);

    Serial.println("after tv");
    t1 = lv_tabview_add_tab(tv, "Main");
    t2 = lv_tabview_add_tab(tv, "1-10V");

    lv_group_add_obj(g, tv);

    //tv_event_cb(tv, LV_EVENT_REFRESH);

#ifdef USE_DALI
    t3 = lv_tabview_add_tab(tv, "Dali");
#endif
    t4 = lv_tabview_add_tab(tv, "About");

    // create tabs
    main_tab_create(t1);
    tab_create_1_10v(t2);

#ifdef USE_DALI
    tab_create_dali(t3);
#endif
    tab_create_about(t4);

    lv_tabview_set_tab_act(tv, 1, LV_ANIM_OFF);

    // // create queue to receive data
    screen_queue = xQueueCreate(MAX_RECEIVE_SCREEN_QUEUE, sizeof(ScreenMessage));

    Serial.println("Screen initialized!");
}

#define SCREEN_UPDATE_TIME_MS 100 // time in milisecconds too update screen

void screen_task(void *pvParameters)
{
    Serial.printf("Screen Task running on core: %d\n", xPortGetCoreID());
    print_current_task_stats();

    BaseType_t xStatus = pdFAIL;
    ScreenMessage msg;
    uint32_t time_till_next_update = 0;
    int iteration = 1;

    for (;;)
    {
        xStatus = xQueueReceive(screen_queue, &msg, SCREEN_UPDATE_TIME_MS / portTICK_PERIOD_MS);
        // delay(SCREEN_UPDATE_TIME_MS);

        // if (iteration % (1000 / SCREEN_UPDATE_TIME_MS) == 0)
        // {

        //     Serial.printf("iteration: %d\n", iteration);
        //     Serial.printf("Time till next update: %d\n", time_till_next_update);
        // }

        // iteration += 1;

        if (xStatus != pdPASS)
        {
            // time_till_next_update = lv_task_handler(); // Call LittleVGL task handler periodically
            lv_task_handler(); // Call LittleVGL task handler periodically
            continue;
        }

        //Serial.println("Message received!");
        switch (msg.msg_type)
        {
        case TestMsg: // used for debug developments
            // screen->tft->drawCentreString("Test #" + String(msg.value_i32), 30, 0, 4); //abs(last_count % 8));
            break;
        case Value1_10vMsg: // print 1-10v
            // screen->tft->drawCentreString("1-10V: " + String(msg.value_float), 30, 30, 2); //abs(last_count % 8));
            set_1_10v_value(msg.value_float * 1000);
            //Serial.printf("1-10V: %f\n", msg.value_float);
            break;
        case LightMsg: // light measurement
                       // Serial.printf("Light meas: %f lux\n", msg.value_float);
            set_lux_value(msg.value_float);
            break;
        case TimeMsg:
            break;
        case EncoderMsg:
            Serial.printf("Encoder message received: count %d\n", msg.value_i32);
            break;
        case UpdateLSI_GET:
            Serial.printf("Update screen get LSI with value: %d\n", msg.value_bool);
            update_screen_lsi_get(msg.value_bool);
            break;
        case UpdateLSI_SET:
            Serial.printf("Update screen set LSI with value: %d\n", msg.value_bool);
            update_screen_lsi_set(msg.value_bool);
            break;
        case UpdateRelay:
            Serial.printf("Update realy with value: %d\n", msg.value_bool);
            update_screen_relay(msg.value_bool);
            break;
        case Update1_10V:
            Serial.printf("Update 1-10V with value: %d\n", msg.value_bool);
            update_screen_1_10v(msg.value_bool);
            break;
        default:
            Serial.println("Unknow message type");
            break;
        }
    }
}
#endif
#endif // USE_SCREEN