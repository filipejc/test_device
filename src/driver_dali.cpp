#define LOG_LOCAL_LEVEL ESP_LOG_WARNING
// logging tag
static const char *TAG = "DALI";

#include "main.h"

#ifdef USE_DALI
#include <Arduino.h> //needed for Serial.println
#include "driver_dali.h"
#include <ArduinoJson.h>

#if DALI_DRIVERS_NUM > 0
#include "driver_server.h"

#define MAX_RECEIVE_QUEUE 128
#define POWER_ON_LEVEL_DEFAULT 254
#define FAILURE_LEVEL_DEFAULT 254
#define PHM_DEFAULT 170
#define MIN_LEVEL_DEFAULT 170
#define MAX_LEVEL_DEFAULT 254
#define FADE_RATE_DEFAULT 7
#define FADE_TIME_DEFAULT 0
#define SHORT_ADDRESS_DEFAULT UNSET_ADDRESS
#define GROUP_ADDRESS_DEFAULT UNSET_ADDRESS

const uint8_t GTIN_DEFAULT[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB};
#define GTIN_LENGTH 6
#define FW_VERSION_LENGTH 2
const uint8_t FW_VERSION_DEFAULT[] = {0x02, 0x01};
#define HW_VERSION_LENGTH 2
const uint8_t HW_VERSION_DEFAULT[] = {0x03, 0x04};
#define SN_LENGTH 8
const uint8_t SN_DEFAULT[] = {0, 0, 0, 0, 0, 0, 0, 1};

DaliDriver *dali_driver;

// Dali driver controller, to handle multiple drivers at same time
DaliDriverController *dali_driver_controller;

#endif

// send frame
bool send_frame(dali_frame_t frame, int delay_time)
{
    delay(delay_time);
    xQueueSend(transmit_queue, &frame, portMAX_DELAY);
    return true;
}



#if DALI_DRIVERS_NUM > 0

void DaliDriver::power_on_load_variables()
{
    ESP_LOGI(TAG, "Load power on values");
    current_light_level = 0;
    power_on_level = read_nvs_uint8(POWER_ON_LEVEL_KEY, POWER_ON_LEVEL_DEFAULT);
    system_failure_level = read_nvs_uint8(FAILURE_LEVEL_KEY, FAILURE_LEVEL_DEFAULT);
    PHM = read_nvs_uint8(PHM_KEY, PHM_DEFAULT);
    min_level = read_nvs_uint8(MIN_LEVEL_KEY, MIN_LEVEL_DEFAULT);
    max_level = read_nvs_uint8(MAX_LEVEL_KEY, MAX_LEVEL_DEFAULT);
    fade_rate = read_nvs_uint8(FADE_RATE_KEY, FADE_RATE_DEFAULT);
    fade_time = read_nvs_uint8(FADE_TIME_KEY, FADE_TIME_DEFAULT);
    short_address = read_nvs_uint8(SHORT_ADDRESS_KEY, SHORT_ADDRESS_DEFAULT);
    group_address = read_nvs_uint8(GROUP_ADDRESS_KEY, GROUP_ADDRESS_DEFAULT);
    read_nvs_uint8(GTIN_KEY, GTIN_DEFAULT, GTIN, GTIN_LENGTH);
    read_nvs_uint8(FW_VERSION_KEY, FW_VERSION_DEFAULT, CONTROL_GEAR_FW, FW_VERSION_LENGTH);

    read_nvs_uint8(HW_VERSION_KEY, HW_VERSION_DEFAULT, HW_VER, HW_VERSION_LENGTH);
    read_nvs_uint8(SN_KEY, SN_DEFAULT, SN, SN_LENGTH);
}

uint8_t DaliDriver::get_id(void)
{
    return _id;
}

void DaliDriver::factory_default_variables()
{
    ESP_LOGI(TAG, "Make factory reset");
    write_nvs_uint8(POWER_ON_LEVEL_KEY, POWER_ON_LEVEL_DEFAULT);
    write_nvs_uint8(FAILURE_LEVEL_KEY, FAILURE_LEVEL_DEFAULT);
    write_nvs_uint8(PHM_KEY, PHM_DEFAULT);
    write_nvs_uint8(MIN_LEVEL_KEY, MIN_LEVEL_DEFAULT);
    write_nvs_uint8(MAX_LEVEL_KEY, MAX_LEVEL_DEFAULT);
    write_nvs_uint8(FADE_RATE_KEY, FADE_RATE_DEFAULT);
    write_nvs_uint8(FADE_TIME_KEY, FADE_TIME_DEFAULT);
    write_nvs_uint8(SHORT_ADDRESS_KEY, SHORT_ADDRESS_DEFAULT);
    write_nvs_uint8(GROUP_ADDRESS_KEY, GROUP_ADDRESS_DEFAULT);
    write_nvs_uint8(GTIN_KEY, GTIN_DEFAULT, GTIN_LENGTH);
    write_nvs_uint8(FW_VERSION_KEY, FW_VERSION_DEFAULT, FW_VERSION_LENGTH);
    write_nvs_uint8(HW_VERSION_KEY, HW_VERSION_DEFAULT, HW_VERSION_LENGTH);
    write_nvs_uint8(SN_KEY, SN_DEFAULT, SN_LENGTH);
}

DaliDriver::DaliDriver(uint8_t id, bool D4i)
{
    ESP_LOGI(TAG, "Create a DaliDriver with id: %d, D4i: %d", id, D4i);
    _id = id;
    is_D4i = D4i;
    device_type_index = 0;
    //ESP_LOGI(TAG, "BACKWARD delay %dms", BACKWARD_FRAME_DELAY);

    // applying default values
    power_on_load_variables();

    initialization_state = INIT_DISABLED;

    // initialize communication state
    communication_state = WAITING_FORWARD_FRAME;
    last_comunication_state_transition_time = 0;

    module_failure_status = 0;
    last_frame.len = 0;
    last_frame.timestamp = 0;

    //queue initilization
    driver_receive_queue = xQueueCreate(MAX_RECEIVE_QUEUE, sizeof(dali_frame_t));

    // create task
    // start receive task
    const char *task_name = (String("DaliDriverTask_") + String(_id)).c_str();

    // setup Dali PSU
    pinMode(DALI_PSU_EN_PIN, OUTPUT);

    xTaskCreate(
        dali_driver_task,   /* Task function. */
        task_name,          /* String with name of task. */
        DALI_TASK_STACK,    /* Stack size in bytes. */
        (void *)this,       /* Parameter passed as input of the task */
        DALI_TASK_PRIORITY, /* Priority of the task. */
        NULL);              /* Task handle. */
}

void DaliDriver::write_nvs_uint8(const char *key, uint8_t data)
{
    char key_dali[MAX_NVS_KEY_LENGHT];
    snprintf(key_dali, MAX_NVS_KEY_LENGHT, "%s_%d", key, _id);
    my_nvs->set_uint8(key_dali, data);
}

void DaliDriver::write_nvs_uint8(const char *key, const uint8_t *data, uint8_t length)
{
    for (int k = 0; k < length; ++k)
    {
        char key_dali[MAX_NVS_KEY_LENGHT];
        snprintf(key_dali, MAX_NVS_KEY_LENGHT, "%s_%d", key, k);
        write_nvs_uint8(key_dali, data[k]);
    }
}

void DaliDriver::read_nvs_uint8(const char *key, const uint8_t *data_default, uint8_t *array, uint8_t length)
{
    for (int k = 0; k < length; ++k)
    {
        char key_dali[MAX_NVS_KEY_LENGHT];
        snprintf(key_dali, MAX_NVS_KEY_LENGHT, "%s_%d", key, k);
        array[k] = read_nvs_uint8(key_dali, data_default[k]);
    }
}

uint8_t DaliDriver::read_nvs_uint8(const char *key, const uint8_t data_default)
{
    char key_dali[MAX_NVS_KEY_LENGHT];
    snprintf(key_dali, MAX_NVS_KEY_LENGHT, "%s_%d", key, _id);
    return my_nvs->get_uint8(key_dali, data_default);
}

String dali_frame_to_string(dali_frame_t frame)
{
    String frame_txt("Frame len: " + String(frame.len) + " timestamp: " + frame.timestamp);
    for (int i = 0; i < frame.len; i++)
    {
        frame_txt += " 0x" + String(frame.bytes[i], HEX);
    }
    return frame_txt;
}

void print_dali_frame(dali_frame_t frame)
{
    // ESP_LOGD(TAG, "%s", dali_frame_to_string(frame).c_str());
}

bool DaliDriver::is_double_frame(dali_frame_t frame)
{
    print_dali_frame(last_frame);
    print_dali_frame(frame);

    // calculate difference for last frame
    unsigned long diff_time = frame.timestamp - last_frame.timestamp;
    if (last_frame.len == 2 && last_frame.bytes[0] == frame.bytes[0] && last_frame.bytes[1] == frame.bytes[1] && diff_time < MAX_TIME_DOUBLE_MS)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// enable  Dali PSU
bool DaliDriverController::set_dali_psu_state(bool state)
{
    bool current_state;
    Serial.printf("Set state Dali PSU: %d\n", state);
    digitalWrite(DALI_PSU_EN_PIN, !state);
    delay(500);
    current_state = digitalRead(DALI_PSU_EN_PIN);
    return current_state;
}

// enable  Dali PSU
bool DaliDriverController::get_dali_psu_state()
{
    bool current_state;
    current_state = digitalRead(DALI_PSU_EN_PIN);
    return current_state;
}

// Simulate a lamp failure, if the DALI master was the lamp state, the response will be an error
void DaliDriver::set_simulate_failure(bool failure)
{
    Serial.printf("Driver: %d, Simulate lamp failure: %d\n", _id, failure);
    lamp_failure = failure;
}

// send message using websockets about the status of the light
void inform_light_level_change(uint16_t driver_id, uint8_t light_level)
{
    StaticJsonDocument<200> doc;
    doc["type"] = "dali_change";
    doc["index"] = driver_id;
    doc["dali_type"] = "light_level";
    doc["data"] = light_level;
    String output;
    serializeJsonPretty(doc, output);
    ws_dali.textAll(output + "");
}

// main function to process frames inside a Dali driver
void DaliDriver::process_frame(dali_frame_t frame)
{
    // state machine to control the status of the Driver, if it should ignore the message or not
    switch (communication_state)
    {
    case WAITING_FORWARD_FRAME:
        break;
    case SENDING_BACKWARD: // transition to WAITING_FORWARD_FRAME if enough time elapsed
        if (millis() - last_comunication_state_transition_time > SENDING_BACKWARD_WAIT_MS)
        {
            communication_state = WAITING_FORWARD_FRAME; // make transition to waiting foward frame
            last_comunication_state_transition_time = millis();
        }
        else
        {
            return;
        }
        break;
    case WAITING_AFTER_FORWARD_FRAME:
        if (millis() - last_comunication_state_transition_time > WAITING_AFTER_FORWARD_FRAME_MS)
        {
            communication_state = WAITING_FORWARD_FRAME; // make transition to waiting foward frame
            last_comunication_state_transition_time = millis();
        }
        else
        {
            return;
        }
        break;
    default:
        ESP_LOGE(TAG, "Unknown communication state %d, please check", communication_state);
        return;
    }

    if (frame.len != 2)
    {
        ESP_LOGE(TAG, "Frame length != 2 is not supported!");
        return;
    }

    // extract address and opcode
    uint8_t address = frame.bytes[0];
    uint8_t opcode = frame.bytes[1];

    // to be used for response if needed
    bool send_response = false;
    dali_frame_t tx_frame;
    uint8_t resp = 0x00;               // to used to compute response
    uint16_t memory_location = 0x0000; // to store memory location

    // check type of addressing
    if (!(address & 0x80))
    { // if MSB is unset, this is short address message
        // ESP_LOGV(TAG, "Short addressing, checking if address match");

        uint8_t frame_short_address = (address & 0x7E) >> 1;
        // check if the address in the frame is the same as the attributed short address
        if (short_address == frame_short_address)
        {
            // ESP_LOGD(TAG, "Short address: %d match!", short_address);
            if ((address & 0x01) == 0x00)
            { // direct arc command
                // ESP_LOGE(TAG, "Direct arc command: to be implemented");
                // TODO: implement checks on the min, max and other limitation, for know just accept the command
                current_light_level = opcode;
                // send message to websocket to update web interface
                inform_light_level_change(_id, current_light_level);
                ESP_LOGI(TAG, "Direct arc command received");
            }
            else
            { // command must see
                switch (opcode)
                {
                case 0x2A: // set max level
                    if (is_double_frame(frame))
                    {
                        if (DTR0 <= min_level)
                        {
                            max_level = min_level;
                        }
                        else if (DTR0 == FRAME_MASK)
                        {
                            max_level = 0xFE; // maximum of max level
                        }
                        else
                        {
                            max_level = DTR0;
                        }
                        write_nvs_uint8(MAX_LEVEL_KEY, max_level);
                    }
                    break;
                case 0x2B: // set MIN level
                    if (is_double_frame(frame))
                    {
                        if (DTR0 <= PHM)
                        {
                            min_level = PHM;
                        }
                        else if (DTR0 > max_level || DTR0 == FRAME_MASK)
                        {
                            min_level = max_level;
                        }
                        else
                        {
                            min_level = DTR0;
                        }
                        write_nvs_uint8(MIN_LEVEL_KEY, min_level);
                    }
                    break;
                case 0x2C: // set system failure level
                    system_failure_level = DTR0;
                    write_nvs_uint8(FAILURE_LEVEL_KEY, system_failure_level);
                    break;
                case 0x2D: // set power on level
                    power_on_level = DTR0;
                    write_nvs_uint8(POWER_ON_LEVEL_KEY, power_on_level);
                    break;
                case 0x2E: // set fade time
                    if (DTR0 > 15)
                    {
                        fade_time = 15;
                    }
                    else
                    {
                        fade_time = DTR0;
                    }
                    write_nvs_uint8(FADE_TIME_KEY, fade_time);
                    break;
                case 0x2F: // set fade rate
                    if (DTR0 > 15)
                    {
                        fade_rate = 0;
                    }
                    else if (DTR0 == 0)
                    {
                        fade_rate = 1;
                    }
                    else
                    {
                        fade_rate = DTR0;
                    }
                    write_nvs_uint8(FADE_RATE_KEY, fade_rate);
                    break;
                case 0x91: // query control gear present
                    ESP_LOGD(TAG, "Query control gear present");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = RESPONSE_YES;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0x92: // query lamp failure
                    ESP_LOGD(TAG, "Query control lamp failure");
                    if (lamp_failure)
                        resp = RESPONSE_YES;
                    else
                        resp = RESPONSE_NO;

                    tx_frame.len = 1;
                    tx_frame.bytes[0] = resp;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0x95: //query reset state
                    ESP_LOGD(TAG, "Query reset state");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = reset_state ? RESPONSE_YES : RESPONSE_NO;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;
                    break;
                case 0x99: // query device type
                case 0xA7: // query next device type
                    ESP_LOGD(TAG, "Query device type");
                    // for now, just answer led module type
                    // construct forward frame
                    tx_frame.len = 1;
                    if (is_D4i)
                    {
                        if (device_type_index >= sizeof(DEVICE_TYPES))
                        {
                            device_type_index = 0;
                        }
                        tx_frame.bytes[0] = DEVICE_TYPES[device_type_index];
                        device_type_index++;
                    }
                    else
                    {
                        tx_frame.bytes[0] = DEVICE_TYPE_6;
                    }

                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0xA0: // query actual power level
                    ESP_LOGD(TAG, "Query actual light level");

                    tx_frame.len = 1;
                    tx_frame.bytes[0] = current_light_level;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);

                    break;

                case 0xA1: // query max level
                    ESP_LOGD(TAG, "Query max light level");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = max_level;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0xA2: // query min level
                    ESP_LOGD(TAG, "Query min light level");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = min_level;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;
                case 0xA3: // query power on level
                    ESP_LOGD(TAG, "Query power on level");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = power_on_level;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;
                case 0xA4: // query system failure on level
                    ESP_LOGD(TAG, "Query system failure on level");
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = system_failure_level;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;
                case 0xA5: // query fade time and fade rate
                    ESP_LOGD(TAG, "Query fade time %d and fade rate %d", fade_time, fade_rate);
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = (fade_time & 0x0F) << 4 | (fade_rate & 0x0F);
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0xC5: // read memory location DTR0, DTR1
                    ESP_LOGD(TAG, "Read memory location: DTR1 %d, DTR0 %d", DTR1, DTR0);
                    memory_location = (DTR1 << 8) + DTR0;

                    if (memory_location == 0x0002) // last accessible memory bank
                    {
                        resp = last_accessible_memory_bank;
                    }
                    else if (memory_location >= 0x0003 && memory_location <= 0x0008) // GTIN
                    {
                        resp = GTIN[memory_location - 0x0003];
                    }
                    else if (memory_location == 0x0009) // CONTROL GEAR FW
                    {
                        resp = CONTROL_GEAR_FW[0];
                    }
                    else if (memory_location == 0x000A)
                    {
                        resp = CONTROL_GEAR_FW[1];
                    }
                    else if (memory_location >= 0x000B and memory_location <= 0x0012) // read serianl number
                    {
                        resp = SN[memory_location - 0x000B];
                    }
                    else if (memory_location == 0x0013) // HW version
                    {
                        resp = HW_VER[0];
                    }
                    else if (memory_location == 0x0014) // HW version
                    {
                        resp = HW_VER[1];
                    }
                    else if (memory_location == 0x19) // number of logic units in a bus. Hardcode to 1
                    {
                        resp = 0x1;
                    }
                    else
                    {
                        ESP_LOGW(TAG, "Memory location %d not recognized!", memory_location);
                    }

                    tx_frame.len = 1;
                    tx_frame.bytes[0] = resp;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0xF0: // query features

                    tx_frame.len = 1;
                    tx_frame.bytes[0] = device_type_6_features;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    break;

                case 0xF1: // if device type == 6 => query failure status
                    // TODO: implement check if last command was to select device type. Assume it was
                    if (device_type == 6)
                    { // previous command should have set device type

                        ESP_LOGD(TAG, "Query device type 6: query failure status");

                        // construct forward frame
                        tx_frame.len = 1;

                        // TODO: make correct implementation on the future
                        //tx_frame.bytes[0] = module_failure_status;

                        // just for testing
                        tx_frame.bytes[0] = lamp_failure ? RESPONSE_YES : RESPONSE_NO;

                        send_response = true;
                        send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                    }
                    else
                    {
                        ESP_LOGW(TAG, "Unsuported device type: %d", device_type);
                    }
                    break;
                default:
                    ESP_LOGW(TAG, "Address: 0x%x, Opcode: 0x%x not recognized, should be analized", address, opcode);
                }
            }
        }
    }
    // group addressing, for now, it is nt doing a lot with this
    else if ((address & 0xE0) == 0x80)
    { // Group addressing
        ESP_LOGI(TAG, "Group addressing, checking if address match");

        uint8_t frame_group_address = (address && 0x1E) >> 1; //
        if (group_address == frame_group_address)
        {
            ESP_LOGD(TAG, "Group %d address match!", group_address);
        }
    }
    else if ((address & 0xE2) == 0xE0)
    { // Broadcast unaddressed
        ESP_LOGI(TAG, "Broadcast unaddressed");
    }
    else if ((address & 0xE2) == 0xE2)
    { // Broadcast all
        ESP_LOGI(TAG, "Broadcast all");
    }
    // should be an special command or reserved
    else if (address <= 0xCB)
    { // Special command
        // ESP_LOGD(TAG, "Special command received!");
        switch (address)
        {
        case 0xA1: // terminate
            ESP_LOGD(TAG, "Terminate command received");
            initialization_state = INIT_DISABLED;
            break;
        case 0xA3: // DTR0 or Data transfer 0
            DTR0 = opcode;
            ESP_LOGD(TAG, "DTR0 %d", DTR0);
            break;

        case 0xA5: // initialize command

            ESP_LOGD(TAG, "Initiliaze command received!");

            if (is_double_frame(frame))
            {
                // 2 frames with equal content received in less than 100ms and ready to evaluate
                ESP_LOGD(TAG, "Evaluation if should react to command");
                if (opcode == 0x00 ||                                                                                           // all control fear shall react
                    ((opcode & 0x81) == 0x00 && short_address != UNSET_ADDRESS && (((opcode >> 1) & 0x2F) == short_address)) || // short address set and equal to requested
                    (opcode == 0xFF && short_address == UNSET_ADDRESS))
                {
                    ESP_LOGD(TAG, "Should react to this command");
                    initialization_state = INIT_ENABLED;
                    initilization_timestamp_ms = millis();
                }
                else
                {
                    ESP_LOGD(TAG, "Should not react to this command");
                }
            }
            break;
        case 0xA7: // randomize
            ESP_LOGI(TAG, "Evaluation of Randomize command");
            if (is_double_frame(frame) && initialization_state == INIT_ENABLED)
            { // just respond if double frame
                //generate random address

                // TODO: implement real random
                long random_address = random(MIN_RANDOM_ADDRESS, MAX_RANDOM_ADDRESS);
                random_addr_h = (random_address & 0xFF0000) >> 16;
                random_addr_m = (random_address & 0x00FF00) >> 8;
                random_addr_l = random_address & 0x0000FF;

                ESP_LOGD(TAG, "Random address generated: 0x%x", (unsigned int)random_address);
            }

            break;
        case 0xA9: // compare
            // ESP_LOGD(TAG, "Compare command received");
            if (initialization_state == INIT_ENABLED)
            {
                long random_address = (random_addr_h << 16) | (random_addr_m << 8) | (random_addr_l);
                long search_address = (search_random_addr_h << 16) | (search_random_addr_m << 8) | (search_random_addr_l);
                if (random_address <= search_address)
                {
                    // ESP_LOGD(TAG, "Random address 0x%x  is less than search address 0x%x, should respond", random_address, search_address);
                    tx_frame.len = 1;
                    tx_frame.bytes[0] = RESPONSE_YES;
                    send_response = true;
                    send_frame(tx_frame, BACKWARD_FRAME_DELAY);
                }
            }
            break;

        case 0xAB: // Withdrawn
            //ESP_LOGD(TAG, "Withdrawn from search");

            if (initialization_state != INIT_DISABLED && // should on respond if initialization state is Disabled
                search_random_addr_h == random_addr_h && // and search address is equal to random address
                search_random_addr_m == random_addr_m &&
                search_random_addr_l == random_addr_l)
            {
                ESP_LOGD(TAG, "Withdrawn from search completed!");

                initialization_state = INIT_WITHDRAWN;
            }
            break;
        case 0xB1: // search address high
            ESP_LOGD(TAG, "Search high Random address");
            search_random_addr_h = opcode;
            break;
        case 0xB3: // search address middle
            ESP_LOGD(TAG, "Search middle Random address");
            search_random_addr_m = opcode;
            break;
        case 0xB5: // search address low
            ESP_LOGD(TAG, "Search low Random address");
            search_random_addr_l = opcode;
            break;
        case 0xB7: // Program short address
            ESP_LOGD(TAG, "Program short address received");
            if ((initialization_state == INIT_ENABLED || initialization_state == INIT_WITHDRAWN) && // should on respond if initialization state is not Disabled
                search_random_addr_h == random_addr_h &&                                            // and search address is equal to random address
                search_random_addr_m == random_addr_m &&
                search_random_addr_l == random_addr_l)
            {
                uint8_t frame_short_address = (opcode & 0x7E) >> 1; //
                if (opcode == 0xFF)
                {
                    ESP_LOGD(TAG, "Deleting the short address");
                    short_address = UNSET_ADDRESS;
                }
                else
                {
                    short_address = frame_short_address;
                    ESP_LOGD(TAG, "Setting short address to %d", short_address);
                }
                write_nvs_uint8(SHORT_ADDRESS_KEY, short_address);
            }
            break;
        case 0xBB: // query short address
            ESP_LOGD(TAG, "Query short address");
            if (initialization_state != INIT_DISABLED && // should on respond if initialization state is Disabled
                search_random_addr_h == random_addr_h && // and search address is equal to random address
                search_random_addr_m == random_addr_m &&
                search_random_addr_l == random_addr_l)
            {
                //ESP_LOGD(TAG, "Answering query short address");

                //construct response frame
                tx_frame.len = 1;
                if (short_address != UNSET_ADDRESS)
                { // if already set, answer with short address
                    tx_frame.bytes[0] = (short_address << 1) | 0x01;
                }
                else
                { // short address not setted
                    //ESP_LOGD(TAG, "No address");
                    //                frame.bytes[0] = 0x0;
                    tx_frame.bytes[0] = FRAME_MASK;
                }
                send_response = true;
                send_frame(tx_frame, BACKWARD_FRAME_DELAY);
            }
            break;
        case 0xC1: // enable device type
            ESP_LOGD(TAG, "Enable device type: %d reveived", opcode);
            device_type = opcode;
            break;

        case 0xC3: // store DTR1
            ESP_LOGD(TAG, "Store DTR1 with %d", opcode);
            DTR1 = opcode;
            break;
        default:
            ESP_LOGW(TAG, "Special command:: 0x%x not treated", address);
        }
    }
    else
    { // reserved commands
        ESP_LOGD(TAG, "Reserved commands, do not respond!");
    }

    // update last command received
    last_frame.timestamp = frame.timestamp;
    last_frame.len = frame.len;
    // update frame data
    for (int i = 0; i < frame.len; i++)
    {
        last_frame.bytes[i] = frame.bytes[i];
    }
    if (send_response)
    { // a backward frame was send
        communication_state = SENDING_BACKWARD;
        last_comunication_state_transition_time = millis();
    }
    else
    {
        communication_state = WAITING_AFTER_FORWARD_FRAME;
        last_comunication_state_transition_time = millis();
    }
}

void dali_driver_task(void *parameters)
{
    StaticJsonDocument<200> doc;
    dali_frame_t frame;
    DaliDriver *driver = (DaliDriver *)parameters;

    bool first_response = true; //

    ESP_LOGI(TAG, "Dali driver task started!");

    while (true)
    {
        //Serial.println("Waiting!");

        xQueueReceive(driver->driver_receive_queue, &frame, portMAX_DELAY);

        // print_dali_frame(frame); // currently it don't print anything.

        // dali drivers index start at 0
        // FIXME: this is a quick hack just to see one  frame as there are more than one driver
        if (driver->get_id() == 0)
        {
            // TODO: add this to other task
            doc.clear();
            doc["len"] = frame.len;
            doc["type"] = "dali";
            doc["dali_type"] = "Dali frame";
            String hex_data("");

            for (int i = 0; i < frame.len; i++)
            {
                hex_data += String(frame.bytes[i], HEX) + " ";
            }
            // Serial.println(hex_data);

            doc["hex_data"] = hex_data;

            // ws_dali.textAll(dali_frame_to_string(frame));
            String output;
            serializeJsonPretty(doc, output);
            // just removed for testing
            // this is causing problems during comissioning
            ws_dali.textAll(output + "");
        }

        driver->process_frame(frame);

        if (frame.len >= 2 && frame.bytes[1] == 0x91)
        {
            dali_frame_t resp_frame;

            // yes
            // resp_frame.len = 1;
            // resp_frame.bytes[0] = RESPONSE_YES;
            // send_frame(resp_frame, BACKWARD_FRAME_DELAY);
            // ESP_LOGI(TAG, "Send get ping");

            // MCU firmware
            resp_frame.len = 1;
            // mcu firmware E0 02 03 E1
            // resp_frame.bytes[0] = 0x7E;
            // send_frame(resp_frame, 1000);

            resp_frame.bytes[0] = 0xE0;
            send_frame(resp_frame, 100);
            resp_frame.bytes[0] = 0x02;
            send_frame(resp_frame, 20);
            resp_frame.bytes[0] = 0x04;
            send_frame(resp_frame, 20);
            resp_frame.bytes[0] = 0xE6;
            send_frame(resp_frame, 20);
            ESP_LOGI(TAG, "Ask MCU");

            first_response = false;
        }

        // dali_driver->process_frame(frame);
        //Serial.println("Frame received");
        //print_dali_frame(element);
    }
}

DaliDriverController::DaliDriverController()
{
    Serial.println("Creating Dali Driver Controller");
}

bool DaliDriverController::factory_reset(uint32_t id)
{
    if (is_id_used(id))
    {

        dali_driver_array[id]->factory_default_variables();
        return true;
    }
    else
    {
        ESP_LOGW(TAG, "Driver %d not found, not possible to factory reset", id);
        return false;
    }
}

bool DaliDriverController::is_id_used(uint32_t id)
{
    // check if already a driver already exists with the same id
    return dali_driver_array.find(id) != dali_driver_array.end();
}

bool DaliDriverController::create_dali_driver(uint32_t id, bool D4i)
{

    // check if already a driver already exists with the same id
    if (is_id_used(id))
    {
        Serial.println("Not possible to create dali driver. Already present");
        return false;
    }
    else // if not present, create a new driver
    {
        DaliDriver *dali_driver = new DaliDriver(id, D4i);
        dali_driver_array[id] = dali_driver;
        dali_comm->register_queue(&dali_driver->driver_receive_queue);
        return true;
    }
}

DaliDriver *DaliDriverController::get_by_id(uint32_t id)
{
    if (is_id_used(id))
    {
        return dali_driver_array[id];
    }
    else
    {
        return NULL;
    }
}

#endif

#endif // USE_DALI