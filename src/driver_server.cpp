#ifdef USE_SERVER

#include "driver_server.h"
#include "SPIFFS.h"
#include "main.h"
#include "driver_dali.h"

// to have Serial and other functions from arduino
#include <Arduino.h>
#include <ArduinoJson.h>

DynamicJsonDocument json_doc(200);

// for wifi and handle REST requests
#include "WiFi.h"

const int MAX_PASSWORDS = 1;

const int MAX_RETRIES = 5;

//#define DEBUG_RX_SERIAL  # Use to debug RX Serial

// credentials for Wifi
// const char ssid[2][16] = {
//     "Hyperion_2",
//     "VFILIPE"};

// const char password[2][20] = {
//     "Light#My#Office",
//     "lamarosa"};

// // credentials for Wifi
const char ssid[1][16] = {
    "Hyperion_2"};

const char password[1][20] = {
    "Light#My#Office"};

//     // credentials for Wifi
// const char ssid[1][16] = {
//     "VFILIPE"};

// const char password[1][20] = {
//     "lamarosa"};

// NTP server to get time
const char *ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

// create websocket on link console
AsyncWebSocket ws("/console");

// open websocket on data
AsyncWebSocket ws_dali("/dali");

// get local time from NTPserver
String get_local_time()
{
    char buffer[20];
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        Serial.println("Failed to obtain time");
        return String("");
    }

    strftime(buffer, 20, "%H:%M:%S", &timeinfo);
    return String(buffer);
}

void get_state_and_index_from_request(AsyncWebServerRequest *request, bool &request_ok, bool &simulate_state, uint16_t &driver_id)
{
    request_ok = false;
    //List all parameters
    int params = request->params();
    for (int i = 0; i < params; i++)
    {
        AsyncWebParameter *p = request->getParam(i);
        if (p->isFile())
        { //p->isPost() is also true
            Serial.printf("FILE[%s]: %s, size: %u\n", p->name().c_str(), p->value().c_str(), p->size());
        }
        else if (p->isPost())
        {
            Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
            if (p->name().compareTo("state") == 0)
            {
                Serial.println("Name is state");
                if (p->value() == "0" or p->value() == "false")
                {
                    request_ok = true;
                    simulate_state = false;
                }
                else if (p->value() == "1" or p->value() == "true")
                {
                    request_ok = true;
                    simulate_state = true;
                }
                else
                {
                    Serial.printf("Invalid parameter received %s\r\n", p->value().c_str());
                    return;
                }
            }
            else if (p->name().compareTo("driver") == 0)
            {
                driver_id = atoi(p->value().c_str());
            }
        }
        else
        {
            Serial.printf("GET[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
    }
};

void get_state_from_request(AsyncWebServerRequest *request, bool &request_ok, bool &request_state)
{
    request_ok = false;
    //List all parameters
    int params = request->params();
    for (int i = 0; i < params; i++)
    {
        AsyncWebParameter *p = request->getParam(i);
        if (p->isFile())
        { //p->isPost() is also true
            Serial.printf("FILE[%s]: %s, size: %u\n", p->name().c_str(), p->value().c_str(), p->size());
        }
        else if (p->isPost())
        {
            Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
            if (p->name().compareTo("state") == 0)
            {
                Serial.println("Name is state");
                if (p->value() == "0" or p->value() == "false")
                {
                    request_ok = true;
                    request_state = false;
                    return;
                }
                else if (p->value() == "1" or p->value() == "true")
                {
                    request_ok = true;
                    request_state = true;
                    return;
                }
                else
                {
                    Serial.printf("Invalid parameter received %s\r\n", p->value().c_str());
                    return;
                }
                // driver_1_10v->set_1_10v_state(set_state);
            }
        }
        else
        {
            Serial.printf("GET[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
    }
    // request->send(200, "text/plain", String(driver_1_10v->get_1_10v_state()));
};

void get_value_from_request_int(AsyncWebServerRequest *request, bool &request_ok, int &request_value)
{
    request_ok = false;
    //List all parameters
    int params = request->params();
    for (int i = 0; i < params; i++)
    {
        AsyncWebParameter *p = request->getParam(i);
        if (p->isPost())
        {
            Serial.printf("POST[%s]: %s\r\n", p->name().c_str(), p->value().c_str());
            // Serial.printf("Compare value: %d\r\n", p->name().compareTo(String("value")));

            if (p->name().compareTo("value") == 0)
            {
                //Serial.println("Name is Value");
                request_value = p->value().toInt();
                request_ok = true;
                return;
            }
            else
            {
                Serial.printf("Invalid parameter received %s\r\n", p->value().c_str());
                return;
            }
            // driver_1_10v->set_1_10v_state(set_state);
        }
    }
};

// handle events on the websocket
void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{

    if (type == WS_EVT_CONNECT)
    {
        Serial.println("Websocket client connection received for Serial");
    }
    else if (type == WS_EVT_DISCONNECT)
    {
        Serial.println("Serial client disconnected");
    }
    else if (type == WS_EVT_DATA)
    {

#ifdef DEBUG_RX_SERIAL
        Serial.println("Data received: ");
#endif

        for (int i = 0; i < len; i++)
        {
#ifdef DEBUG_RX_SERIAL
            Serial.print(char(data[i]));
#endif
            Serial2.print(char(data[i]));
            //Serial.print("|");
        }
        //client->text("message received!\r\n");

        Serial.println();
    }
}

// handle events on the websocket on dali
void onWsDaliEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{

    if (type == WS_EVT_CONNECT)
    {

        Serial.println("Dali Websocket client connection received");
    }
    else if (type == WS_EVT_DISCONNECT)
    {
        Serial.println("Dali websocket Client disconnected");
    }
    else if (type == WS_EVT_DATA)
    {
        Serial.println("Data received: ");

        for (int i = 0; i < len; i++)
        {
            Serial.print(char(data[i]));
        }
        // client->text("message received!\r\n");

        Serial.println();

        DeserializationError error = deserializeJson(json_doc, data);
        if (error)
        {
            Serial.print("parseObject() failed with code: ");
            Serial.println(error.c_str());
            return;
        }
        const char *cmd_type = json_doc["type"];

#ifdef USE_DALI
        if (strcmp("dali_cmd", cmd_type) == 0)
        {
            Serial.println("Dali command received");
            uint8_t dali_array_len = json_doc["len"];
            dali_frame_t data_frame;
            data_frame.len = 1;
            Serial.printf("Data received with len %d\n", dali_array_len);
            for (int k = 0; k < dali_array_len; k++)
            {
                uint8_t data_point = json_doc["data"][k];
                data_frame.bytes[0] = data_point;
                Serial.printf("Data[%d]: %d\n", k, data_point);
                send_frame(data_frame, 20);
            }
        }

        else
        {
            Serial.printf("Command %s not recognized\n", json_doc["type"]);
        }
#endif
    }
}

void server_task(void *pv_parameters)
{
    // Initialize SPIFFS
    if (!SPIFFS.begin(true))
    {
        Serial.println("An Error has occurred while mounting SPIFFS");
        return;
    }
    // create server on  port 80
    AsyncWebServer server(80);
    // connect to WIFI
    // connect Wifi

    while (true)
    {
        for (int i = 0; i < MAX_PASSWORDS; i++)
        {
            Serial.printf("Trying to connect to SSID: %s, password: %s\n", ssid[i], password[i]);
            // connect to WIFI
            WiFi.begin(ssid[i], password[i]);

            delay(1000); // just give sometime to connect
            for (int try_n = 0; try_n < MAX_RETRIES; try_n++)
            {
                if (WiFi.status() == WL_CONNECTED)
                {
                    break;
                }

                delay(2000);
                Serial.printf("Connecting to WiFi... try #%d\n", try_n);
            }
        }
        if (WiFi.status() == WL_CONNECTED)
        {

            Serial.printf("Connected to IP: %s\n", WiFi.localIP().toString().c_str());
            break;
            //m5.Lcd.drawString("Wifi connected!", 160, 20);
        }
        else
        {
            Serial.printf("Could not connect to wifi network!");
            //m5.Lcd.drawString("Wifi not connected!", 160, 20);
        }
    }

    Serial.printf("Connected to IP: %s\n", WiFi.localIP().toString().c_str());

    //init and get the time
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    get_local_time();

    // connect websocket to console
    ws.onEvent(onWsEvent);
    server.addHandler(&ws);

    // connect websocket to dali
    ws_dali.onEvent(onWsDaliEvent);
    server.addHandler(&ws_dali);

    // get digital in state
    server.on("/test_connection", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  Serial.println("Test connectin request received!");
                  request->send(200, "text/plain", "test work correctly!");
                  Serial.println("Response for test connection send!.");
              });

    // get digital in state
    server.on("/din_state", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  // print statistics
                  print_current_task_stats();

                  request->send(200, "text/plain", String(digital_in->get_state()));
              });

#ifdef USE_DALI

    server.on("/dali_enable", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  int paramsNr = request->params();
                  uint16_t driver_index;
                  Serial.println(paramsNr);
                  DaliDriver *driver;
                  String response = "Bad, request, please check";

                  for (int i = 0; i < paramsNr; i++)
                  {

                      AsyncWebParameter *p = request->getParam(i);
                      Serial.print("Param name: ");
                      Serial.println(p->name());
                      Serial.print("Param value: ");
                      Serial.println(p->value());
                      Serial.println("------");
                      if (p->name().compareTo("index") == 0)
                      {
                          driver_index = atoi(p->value().c_str());
                          driver = dali_driver_controller->get_by_id(driver_index);
                          if (driver == NULL)
                          {
                              Serial.println("Invalid driver index");
                          }
                          else
                          {
                              response = driver->current_light_level == 0 ? "0" : "1";
                          }
                      }
                  }
                  request->send(200, "text/plain", response);
              });

    // Dali simulate failure
    server.on("/simulate_failure", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  Serial.printf("Simulate failure in Driver");
                  bool request_ok;
                  bool simulate_state;
                  uint16_t driver_index;
                  String response;

                  get_state_and_index_from_request(request, request_ok, simulate_state, driver_index);
                  if (request_ok)
                  {
                      Serial.printf("Setting driver: %d to state: %d", driver_index, simulate_state);
                      DaliDriver *driver;
                      driver = dali_driver_controller->get_by_id(driver_index);
                      if (driver == NULL)
                      {
                          response = "Invalid driver";
                      }
                      else
                      {
                          driver->set_simulate_failure(simulate_state);
                          response = "State changed";
                      }
                  }
                  else
                  {
                      response = "Bad request for Dali simulate failure";
                  }

                  request->send(200, "text/plain", response);
              });

    // Set dali power supply
    server.on("/dali_psu", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  Serial.printf("Get dali state\n");
                  bool current_state;
                  current_state = dali_driver_controller->get_dali_psu_state();
                  request->send(200, "text/plain", current_state ? "0" : "1");
              });

    // Set dali power supply
    server.on("/dali_psu", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  Serial.printf("Set dali state\n");
                  bool request_ok;
                  bool state;
                  bool current_state;
                  String response;

                  get_state_from_request(request, request_ok, state);
                  if (request_ok)
                  {
                      Serial.printf("Setting dali PSU state: %d\n", state);
                      current_state = dali_driver_controller->set_dali_psu_state(state);
                      response = String(current_state);
                  }
                  else
                  {
                      response = "Bad request for Dali PSU enable";
                  }

                  request->send(200, "text/plain", response);
              });

#endif // USE_DALI

    // set state of digital in
    server.on("/din_state", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  bool request_ok;
                  bool request_state;
                  String response;

                  // get data from request
                  get_state_from_request(request, request_ok, request_state);

                  if (request_ok)
                  {
                      digital_in->set_state(request_state);
                      response = digital_in->get_state();
#ifdef USE_SCREEN
                      inform_screen_on_change_bool(UpdateLSI_SET, !request_state);
#endif //
                  }
                  else
                  {
                      response = "Bad request, check form data";
                  }
                  request->send(200, "text/plain", response);
              });

    // respond to Get requests on URL /on_off
    server.on("/on_off", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  Serial.println("Receiving request on-off get!");
                  String response(relay_on_off->get_state());
                  request->send(200, "text/plain", response);
              });

    // respond to GET requests on URL /heap
    server.on("/on_off", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  bool request_ok;
                  bool request_state;
                  String response;

                  // get data from request
                  get_state_from_request(request, request_ok, request_state);

                  if (request_ok)
                  {
                      relay_on_off->set_state(request_state);
                      response = relay_on_off->get_state();
#ifdef USE_SCREEN
                      inform_screen_on_change_bool(UpdateRelay, relay_on_off->get_state());
#endif // USE_SCREEN end
                  }
                  else
                  {
                      response = "Bad request, check form data";
                  }
                  request->send(200, "text/plain", response);
              });

    // respond to GET requests on URL /1_10v_value
    server.on("/1_10v_value", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(200, "text/plain", String(driver_1_10v->read_1_10v())); });

#ifdef USE_USE_1_10V_GEN
    // respond to GET requests on URL /1_10v_state
    server.on("/1_10v_state", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(200, "text/plain", String(driver_1_10v->get_state())); });
    // respond to PUT requests on URL /1_10v_state
    server.on("/1_10v_state", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  bool request_ok;
                  bool request_state;
                  String response;

                  // get data from request
                  get_state_from_request(request, request_ok, request_state);

                  Serial.println("1_10V trying to set");

                  if (request_ok)
                  {
                      driver_1_10v->set_state(request_state);
                      response = driver_1_10v->get_state();
#ifdef USE_SCREEN
                      inform_screen_on_change_bool(Update1_10V, driver_1_10v->get_state());
#endif // USE_SCREEN end
                  }
                  else
                  {
                      response = "Bad request, check form data";
                  }
                  request->send(200, "text/plain", response);
              });
#endif

    // support for remote control of the LED
    // respond to GET requests on URL /1_10v_value
    server.on("/led_value", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(200, "text/plain", String(photocell_led->get_level())); });

#ifdef USE_LIGHT_SENSOR
    server.on("/light", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  long lux = light_sensor->read_lux();
                  //Serial.printf("Server: ligh: %l lux\n", lux);
                  request->send(200, "text/plain", String(lux));
              });
#endif

    // respond to GET requests on URL /1_10v_state
    server.on("/led_value", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  bool request_ok;
                  int request_value;
                  String response;

                  // get data from request
                  get_value_from_request_int(request, request_ok, request_value);

                  if (request_ok)
                  {
                      photocell_led->set_level(request_value);
                      response = photocell_led->get_level();
                  }
                  else
                  {
                      response = "Bad request, check form data";
                  }
                  request->send(200, "text/plain", response);
              });

#ifdef USE_DALI
    // respond to GET requests on URL /1_10v_state
    server.on("/dali_factory_reset", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                  Serial.println("Dali resquest on");
                  bool request_ok;
                  int request_value;
                  bool resp;
                  String response;

                  // get data from request
                  get_value_from_request_int(request, request_ok, request_value);

                  if (request_ok)
                  {
                      resp = dali_driver_controller->factory_reset(request_value);
                      response = String(resp);
                  }
                  else
                  {
                      response = "Bad request, check form data";
                  }
                  request->send(200, "text/plain", response);
              });

#endif // USE_DALI

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                  //Send index.htm with default content type
                  request->send(SPIFFS, "/index.html");
              });

    //start to serve static files
    server.serveStatic("/", SPIFFS, "/");

    //increase priority for the async tcp task
    // TaskHandle_t asynctcp_handle = xTaskGetHandle("asynctcp");

    // if (asynctcp_handle != NULL)
    // {
    //     Serial.println("Increase priority of the async tcp task");
    //     vTaskPrioritySet(asynctcp_handle, SERVER_TASK_PRIORITY);
    // }

    // start to accept requests
    server.begin();
    Serial.println("Server started!");

    while (true)
    {
        delay(1);
        Serial.println("Server task stopped because asynctcp running in it own task");
        vTaskSuspend(NULL);
    }
}

#endif