#ifdef USE_1_10V
#include <Arduino.h>
#include "driver_1_10v.h"

Driver_1_10v::Driver_1_10v()
{
    Serial.println("Creating a 1_10V driver");

    // configure pins

    pinMode(PIN_1_10V_FEEDBACK, INPUT);

#ifdef USE_USE_1_10V_GEN
    pinMode(PIN_1_10V_ENABLE, OUTPUT);

    //disable 1-10V
    set_state(false);
#endif
}

#ifdef USE_USE_1_10V_GEN
void Driver_1_10v::set_state(bool state)
{
    _state = state;

    Serial.println("Setting 1-10V state to " + String(state ? "enable" : "disabled"));

    //inverted due to circuit
    digitalWrite(PIN_1_10V_ENABLE, !state);
}
#endif

bool Driver_1_10v::get_state()
{
    return _state;
}

float Driver_1_10v::read_1_10v()
{
    int analogValue = analogRead(PIN_1_10V_FEEDBACK);

    float value = analogValue * GAIN_1_10V;
    // uncomment line below for debug
    //Serial.printf("Pin: %d, Raw value: %d, Current 1-10V: %fV\n", PIN_1_10V_FEEDBACK, analogValue, value);
    return value;
}

#ifdef USE_LIGHT_SENSOR
LightSensor::LightSensor()
{
    Serial.println("Light sensor start!");
    Wire.begin();
    TSL2561.init(); // Grove light LightSensor
}

long LightSensor::read_lux()
{
    // Serial.println("Reading light sensor");
    long lux = TSL2561.readVisibleLux();
    return lux;
}
#endif // LIGHT Sensor

#ifdef USE_USE_1_10V_GEN
void Driver_1_10v::test()
{
    set_state(true);
    delay(100);
    read_1_10v();
    delay(1000);
    set_state(false);
    delay(100);
    read_1_10v();
    delay(1000);
}
#endif

#endif