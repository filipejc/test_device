#ifdef USE_RELAY

#include "driver_relay.h"
#include <Arduino.h>

Relay::Relay(int pin_number, bool state)
{
    _pin_number = pin_number;
    _state = state;
    Serial.printf("Initializing pin %d with state %d\r\n", pin_number, state);

    // configure pin as an OUTPUT
    pinMode(pin_number, OUTPUT);
    // set drive strength to max
    if (gpio_set_drive_capability((gpio_num_t)pin_number, GPIO_DRIVE_CAP_3) == ESP_OK)
    {
        Serial.println("Pin set to max drive strength");
    }
    else
    {
        Serial.println("Error setting max drive strength");
    }

    set_state(state);
};

void Relay::set_state(bool state)
{
    Serial.printf("Setting relay %d with state %d\r\n", _pin_number, state);
    _state = state;
    digitalWrite(_pin_number, !state);
}

bool Relay::get_state()
{
    return _state;
}

#endif