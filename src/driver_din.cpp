#ifdef USE_DIGITAL

#include "driver_din.h"
#include "main.h"
#include <Arduino.h>

// can trigger more than once for the same transition. Especially from 1 to 0.
void IRAM_ATTR digital_in_ISR(void)
{
    //TODO: check implementation in HW
    //Serial.println("Digital in changed!");

#ifdef USE_SCREEN
    inform_screen_on_change_bool(UpdateLSI_GET, digital_in->get_state());
#endif // USE_SCREEN end
}

bool DIN::get_state(void)
{
    bool state = digitalRead(_read_pin_number);
    // Serial.printf("Read Din on pin %d, state: %d\r\n", _read_pin_number, state);
    return state;
}

void DIN::set_state(bool state)
{
    Serial.printf("Set Din on pin %d, state: %d\r\n", _set_pin_number, !state);
    pinMode(_set_pin_number, OUTPUT);
    digitalWrite(_set_pin_number, !state);
    delay(50);
    Serial.printf("Pin status: %d\r\n", digitalRead(_set_pin_number));
}

DIN::DIN(int read_pin_number, int set_pin_number)
{
    Serial.printf("Creating a Digital Input interface. Read pin: %d. Set pin: %d\r\n", read_pin_number, set_pin_number);
    _read_pin_number = read_pin_number;
    _set_pin_number = set_pin_number;

    // configure pins

    pinMode(read_pin_number, INPUT);
    digitalWrite(read_pin_number, LOW); // turn on pullup resistors
    pinMode(set_pin_number, OUTPUT);

    // attach interrupt
    attachInterrupt(read_pin_number, digital_in_ISR, CHANGE);
}

#endif