#ifdef USE_NVS

#define LOG_LOCAL_LEVEL ESP_LOG_WARNING
// logging tag
static const char *TAG = "NVS";

#include "driver_nvs.h"
#include <Arduino.h>
#include "main.h"
#include <string.h>

void write_nvs_task(void *parameters)
{
    ESP_LOGI(TAG, "Write NVS task stater");

    NVS_memory data;

    while (true)
    {
        //Serial.println("Waiting!");

        xQueueReceive(my_nvs->nvs_write_queue, &data, portMAX_DELAY);

        Serial.println("object received!");

        if (data.NVS_TYPE == NVS_UINT8)
        {
            my_nvs->set_uint8_internal(data.key, (uint8_t)data.buffer[0]);
        }
        else
        {
            ESP_LOGE(TAG, "DATA type not recognized!");
        }
        //print_dali_frame(element);
    }
}

MyNVS::MyNVS()
{
    Serial.println("Initializing Non-volatile storage!");

    // Initialize NVS
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    // Open
    Serial.print("Opening Non-Volatile Storage (NVS) handle... ");
    err = nvs_open("storage", NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        Serial.printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        Serial.printf("Done\n");
    }

    // create queue
    //queue initilization
    nvs_write_queue = xQueueCreate(MAX_RECEIVE_NVS_QUEUE, sizeof(NVS_memory));

    // create task to receive
    // start receive task
    xTaskCreate(
        write_nvs_task,   /* Task function. */
        "Write_NVS_task", /* String with name of task. */
        20000,            /* Stack size in bytes. */
        NULL,             /* Parameter passed as input of the task */
        1,                /* Priority of the task. */
        NULL);            /* Task handle. */
};

void MyNVS::save_changes()
{
    ESP_LOGD(TAG, "Saving changes in NVM");
    esp_err_t err = nvs_commit(my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Saving changes failed");
    }
    else
    {
        ESP_LOGI(TAG, "Saving changes completed!");
    }
}

void MyNVS::set_uint8(const char *key, uint8_t data)
{
    ESP_LOGD(TAG, "Setting %s to %d", key, data);
    NVS_memory element;
    element.NVS_TYPE = NVS_UINT8;
    element.buffer[0] = data;
    strncpy(element.key, key, MAX_NVS_KEY_LENGHT);
    // TODO: set ticks to a more real number
    xQueueSend(nvs_write_queue, &element, 10);
}

void MyNVS::set_uint8_internal(const char *key, uint8_t data)
{
    ESP_LOGD(TAG, "Setting %s to %d", key, data);
    esp_err_t err = nvs_set_u8(my_handle, key, data);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Couldn't not store % with value %d in NVS\n", key, data);
    }
    else
    {
        ESP_LOGD(TAG, "Setting with sucess\n");
    }
    save_changes();
}

uint8_t MyNVS::get_uint8(const char *key, uint8_t default_value)
{
    uint8_t data;
    esp_err_t err = nvs_get_u8(my_handle, key, &data);
    switch (err)
    {
    case ESP_OK:
        ESP_LOGD(TAG, "%s = %d", key, data);
        break;
    case ESP_ERR_NVS_NOT_FOUND:
        ESP_LOGD(TAG, "The value is not initialized yet!\n");
        data = default_value;
        set_uint8_internal(key, data);
        break;
    default:
        ESP_LOGE(TAG, "Error (%s) reading!", esp_err_to_name(err));
    }
    return data;
}

#endif