#ifdef USE_LED

#include "driver_led.h"
#include <Arduino.h>

bool Driver_LED::set_level(int level)
{
    if (level > 100 or level < 0)
    {
        Serial.printf("Invalid level: %d\r\n", level);
        return false;
    }
    else
    {
        int PWM_value = ((float)level / 100.0) * 255.0;
        Serial.printf("Setting PWM to %d\r\n", PWM_value);
        ledcWrite(PWM_LED_CHANNEL, PWM_value);
        _current_level = level;
        return true;
    }
}

int Driver_LED::get_level(void)
{
    return _current_level;
}

Driver_LED::Driver_LED(int led_pin_number)
{
    Serial.printf("Create PWM LED interface on pin %d\r\n", led_pin_number);
    // setup led PWM channel
    ledcSetup(PWM_LED_CHANNEL, PWM_LED_FREQ, PWM_LED_RESOLUTION);
    // attach the Channel to GPIO
    ledcAttachPin(led_pin_number, PWM_LED_CHANNEL);
    this->set_level(0);
}

#endif