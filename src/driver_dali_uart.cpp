#include "driver_dali_uart.h"
#include <Arduino.h>
#include "freertos/FreeRTOS.h"
#include "dali_comm.h"

// extern "C"
// {
// #include "crypto/base64.h"
// }

#ifdef USE_DALI_UART

static const char *TAG = "Dali UART";

QueueHandle_t dali_uart_receive_queue;

const char START_BYTE = 1;
const char END_BYTE = 4;
const unsigned long maximum_inter_byte_time = 1000; // maximum time betwen bytes

DaliUart::DaliUart()
{
    dali_uart_receive_queue = xQueueCreate(MAX_UART_DALI_QUEUE, sizeof(dali_frame_t));
}

enum ReceiveState
{
    WaitStartByte = 0,
    WaitLen,
    ReceiveData,
    WaitCRC,
    WaitEndByte
};

#define BUFFER_SIZE 100

void data_uart_task(void *parameter)
{
    String buffer;
    char cbuffer[BUFFER_SIZE]; // char buffer
    char message[BUFFER_SIZE]; // store decode information
    int idx = 0;               // save position in the buffer
    dali_frame_t rx_frame;     // to receive a frame
    BaseType_t xStatus;
    char byte;
    int state = WaitStartByte; // store state for recevi
    int expected_len = 0;      // store expected length
    int CRC = 0;               // store value of received CRC
    static unsigned long last_byte_time = 0;
    dali_frame_t tx_frame; // to sent the frame

    ESP_LOGI(TAG, "DALI UART Transmit task started!");

    // run forever
    for (;;)
    {
        while (Serial.available())
        {
            if (Serial.readBytes(&byte, 1))
            {
                // Serial.print("Received: '");
                if (debug_dali_uart)
                {
                    Serial.print(byte);
                }
                // Serial.print("'\n");
                if ((millis() - last_byte_time) > maximum_inter_byte_time)
                {
                    state = WaitStartByte;
                }
                last_byte_time = millis();

                switch (state)
                {
                case WaitStartByte:
                    if (byte == START_BYTE)
                    {
                        if (debug_dali_uart)
                        {
                            ESP_LOGD(TAG, "START_BYTE received");
                        }
                        state = WaitLen;
                    }
                    break;

                case WaitLen:
                    // initial receive data state
                    expected_len = (int)byte;
                    buffer = "";
                    memset(cbuffer, 0, sizeof(buffer));
                    idx = 0;
                    state = ReceiveData;
                    break;

                case ReceiveData:
                    cbuffer[idx] = byte;
                    idx++;
                    buffer += byte;

                    if (idx == expected_len)
                    {
                        state = WaitCRC;
                    }

                    break;

                case WaitCRC:
                    CRC = byte;
                    state = WaitEndByte;
                    break;

                case WaitEndByte:
                    if (byte == END_BYTE)
                    {
                        // deconde base64 message
                        size_t outputLength;
                        unsigned char *decoded = base64_decode((const unsigned char *)cbuffer, strlen(cbuffer), &outputLength);

                        if (debug_dali_uart)
                        {
                            ESP_LOGI(TAG, "Message received!");
                            Serial.printf("Message len: %d, message: %s", outputLength, decoded);
                        }

                        tx_frame.len = 1;

                        // send data
                        for (int k = 0; k < outputLength; k++)
                        {
                            uint8_t data_point = decoded[k];
                            tx_frame.bytes[0] = data_point;

                            // print value to send
                            if (debug_dali_uart)
                            {
                                Serial.printf("Data[%d]: %d\n", k, data_point);
                            }
                            delay(20);
                            xQueueSend(transmit_queue, &tx_frame, portMAX_DELAY);
                        }
                        if (debug_dali_uart)
                        {
                            ESP_LOGI(TAG, "Data send");
                        }
                        free(decoded);
                    }
                    state = WaitStartByte;
                    break;
                default:
                    ESP_LOGW(TAG, "ERROR in state");
                    state = WaitStartByte;
                    break;
                }
            };
        }

        xStatus = xQueueReceive(dali_uart_receive_queue, &rx_frame, 10 / portTICK_PERIOD_MS);
        if (xStatus == pdPASS)
        {
            if (debug_dali_uart)
            {
                ESP_LOGD(TAG, "Frame received!");
            }

            // construct the message
            char rx_message[100]; // message to send to UART
            uint8_t rx_idx = 0;
            size_t outputLength;
            rx_message[rx_idx] = START_BYTE; // start byte
            rx_idx++;

            unsigned char *encoded = base64_encode((const unsigned char *)rx_frame.bytes, rx_frame.len, &outputLength);

            // print encoded message
            if (debug_dali_uart)
            {
                Serial.printf("RX len: %d, Encoded: len: %d, TXT='%s'\n", rx_frame.len, outputLength, encoded);
            }

            rx_message[rx_idx] = (char)outputLength;
            rx_idx++;
            uint8_t calculate_crc = 0;
            for (int i = 0; i < outputLength; i++)
            {
                rx_message[rx_idx] = encoded[i];
                rx_idx++;
                calculate_crc ^= (uint8_t)encoded[i];
            }

            rx_message[rx_idx] = (char)calculate_crc; // TODO: add CRC algorithm
            rx_idx++;
            rx_message[rx_idx] = END_BYTE; // add end byte
            rx_idx++;
            rx_message[rx_idx] = '\x00'; // end message
            Serial.print(rx_message);

            free(encoded); // clear message encoded
        }
    }
};

#endif // USE_DALI_UART