#ifndef DALI_COMM_H
#define DALI_COMM_H

#include <Arduino.h>

// max number of queues
#define MAX_DALI_COMM_QUEUES 100

// control operating modes
//#define TEST_TX
#define debug_tx false // if true, print debug messages for tx
#define debug_rx false // if true, print debug messages for rx

// to control if data is inverted or not in manchester encoding
// it is inverted due to the optocoupler
#define RX_FALLING_VALUE 1
#define TX_RISING_VALUE 0

// to control if data is inverted during transmittion. For non-invertion, comment the next line
#define TX_TRANSMIT_INVERTED true

#ifdef TX_TRANSMIT_INVERTED
const uint8_t ONE_FIRST_HALF = 0;
const uint8_t ONE_SECOND_HALF = 1;
const uint8_t ZERO_FIRST_HALF = 1;
const uint8_t ZERO_SECOND_HALF = 0;
#else
const uint8_t ONE_FIRST_HALF = 1;
const uint8_t ONE_SECOND_HALF = 0;
const uint8_t ZERO_FIRST_HALF = 0;
const uint8_t ZERO_SECOND_HALF = 1;
#endif

#define RX_TRANSMIT_INVERTED true

#ifdef RX_TRANSMIT_INVERTED
const uint8_t RX_ONE_FIRST_HALF = 0;
const uint8_t RX_ONE_SECOND_HALF = 1;
const uint8_t RX_ZERO_FIRST_HALF = 1;
const uint8_t RXZERO_SECOND_HALF = 0;
#else
const uint8_t RX_ONE_FIRST_HALF = 1;
const uint8_t RX_ONE_SECOND_HALF = 0;
const uint8_t RX_ZERO_FIRST_HALF = 0;
const uint8_t RX_ZERO_SECOND_HALF = 1;
#endif

// define max sizes for receive and transmit queues
const int MAX_RECEIVE_QUEUE = 128;
const int MAX_TRANSMIT_QUEUE = 128;

// define max backwar bytes
const int DALI_BACKWARD_MAX_BYTES = 10;

// define max number of transmit bits (1 start bit, + 8x # bytes + 2 stop bits
const int MAX_BACKWARD_BITS = 1 + 8 * DALI_BACKWARD_MAX_BYTES + 2;

// part of the reception: define charactherists of receive frame
const int MIN_PULSE_WIDTH = 30;     // * 10us, minimum pulse width
const int MAX_PULSE_WIDTH = 65;     // * 10us, maximum pulse width
const int NOMINAL_PULSE_WIDTH = 43; // * 10us, nominal pulse width

const int MAX_BITS_RECEIVED = 256;

// define max number of bytes in a receive frame (forward frame)
const int DALI_FORWARD_MAX_BYTES = 10;

// DALI half bit time
// const int DALI_HALF_BIT_TIME_US = 417; // 1 / 1200 / 2 => 416.66 us
#define DALI_HALF_BIT_TIME_US 417

#define MIN_TRANSMISSION_INTERVAL 20 // MS: minimum time between transmissions

// define frame
typedef struct dali_frame_t
{
    uint8_t bytes[DALI_FORWARD_MAX_BYTES]; //data in the frame
    uint8_t len;                           // length in bytes of the frame
    unsigned long timestamp;               // time stamp when the frame was received
} dali_frame_t;

//
void receive_task(void *);
void transmit_task(void *);
// functions to decode data
void receive_data(uint32_t *data, size_t len);
extern QueueHandle_t receive_queue;
extern QueueHandle_t transmit_queue;

class DALI_COMM
{
public:
    DALI_COMM(int dali_rx_pin, int dali_tx_pin, UBaseType_t task_tx_priority, UBaseType_t task_rx_priority,
              bool dali_tx_idle_level, int dali_tx_stack_size, int dali_rx_tack_size);
    void register_queue(QueueHandle_t *);
    void inform_queues(dali_frame_t);

    // RMT receive and transmit objects
    rmt_obj_t *rmt_recv = NULL;
    rmt_obj_t *rmt_trans = NULL;

private:
    UBaseType_t _task_tx_priority;
    UBaseType_t _task_rx_priority;
    int queue_number = 0;
    QueueHandle_t *queue_array[MAX_DALI_COMM_QUEUES];
};

// Dali communications
//extern DALI_COMM *dali_comm;

#endif