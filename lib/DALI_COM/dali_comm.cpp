#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
// logging tag
static const char *TAG = "DALI_COM";

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp32-hal.h"
#include "driver/rmt.h"

#include "dali_comm.h"

// uncomment next line if you want debug messages
#define DEBUG

// variable to store the dali driver
// define max number of drivers
// const int MAX_DALI_DRIVERS = 1;
// DaliDriver dali_drivers[MAX_DALI_DRIVERS];
// int current_dali_count = 0;

QueueHandle_t receive_queue;
QueueHandle_t transmit_queue;

DALI_COMM *dali_comm;

#define DEBUG_PIN 21

void toogle_debug_pin()
{
    pinMode(DEBUG_PIN, OUTPUT);
    digitalWrite(DEBUG_PIN, LOW);
    digitalWrite(DEBUG_PIN, HIGH);
    digitalWrite(DEBUG_PIN, LOW);
}

void receive_data(uint32_t *data, size_t len, void *arg)
{
    bool valid = true;
    rmt_data_t *items = (rmt_data_t *)data;
    rmt_data_t *it = NULL;

#ifdef DEBUG
    // Serial.printf("ParseRMT: %d\n", len);
    // toogle_debug_pin();
#endif

    int previous_central_time = 0;
    int current_time = 0;
    int delta_time = 0;

    int bit_array[MAX_BITS_RECEIVED]; // store bits received
    int bit_index = 0;

    for (size_t i = 0; i < len; i++)
    {

        it = &items[i];
#ifdef DEBUG
        // Serial.printf("Duration 0: %d, level 0: %d, duration 1: %d, level 1: %d", it->duration0, it->level0, it->duration1, it->level1);
#endif
        // receive of start bit
        if (i == 0)
        {
            if (it->duration0 > MIN_PULSE_WIDTH && it->duration0 < MAX_PULSE_WIDTH && it->duration1 > MIN_PULSE_WIDTH && it->duration1 < 2 * MAX_PULSE_WIDTH && it->level0 == RX_ONE_FIRST_HALF && it->level1 == RX_ONE_SECOND_HALF)
            {
#ifdef DEBUG
                // Serial.println("Start bit found!");
#endif
                previous_central_time = it->duration0;
                current_time = it->duration0 + it->duration1;
            }
            else
            {
                // ESP_LOGW(TAG, "Don't start with start bit, should be an error");
                //  toogle_debug_pin();
                return;
            }
        }
        else
        {
            if (it->level0 == RX_ONE_FIRST_HALF && it->level1 == RX_ONE_SECOND_HALF)
            { // rising edge should check if it is one half bit time or two from previous change
                // evaluation of transistion of previous 0 to 1
                delta_time = current_time - previous_central_time;
                if (delta_time > MAX_PULSE_WIDTH && delta_time < 2 * MAX_PULSE_WIDTH)
                { // # transition on middle of bit falling
                    bit_array[bit_index++] = TX_RISING_VALUE;

                    previous_central_time = current_time;
                }

                current_time += it->duration0;

                // # evaluation of 1 to 0 transition
                delta_time = current_time - previous_central_time;

                if (delta_time > MAX_PULSE_WIDTH && delta_time < 2 * MAX_PULSE_WIDTH)
                { //  # transition on middle of bit falling
                    bit_array[bit_index++] = RX_FALLING_VALUE;
                    previous_central_time = current_time;
                }
                current_time += it->duration1;
            }
        }
        if (bit_index >= MAX_BITS_RECEIVED - 1)
        { // -1 because 2 bits can generated in one iteration, this prevents to check after each byte is added
            // ESP_LOGE(TAG, "No more space to receive bits");
            break;
        }
    }

    if (!valid)
    {
        // ESP_LOGE(TAG, "Error validating the conversion");
        return;
    }
    // Serial.print("Result: ");
    // Serial.println(bit_index);

    // conversion of bit array to byte array
    dali_frame_t frame;
    frame.len = 0;

    // FIXME: add real time stamp. This timestamp is used to calculate if a double frame was received
    frame.timestamp = millis();
    for (int i = 0; i < bit_index; i++)
    {
        if (i % 8 == 0)
        {
            frame.len++;
            if (frame.len > DALI_FORWARD_MAX_BYTES)
            {
                // ESP_LOGE(TAG, "WARNING: MAX number of bytes receive but more data available");
                break;
            }
            frame.bytes[frame.len - 1] = 0;
        }
        else
        {
            frame.bytes[frame.len - 1] <<= 1;
        }
        frame.bytes[frame.len - 1] |= bit_array[i]; // start bit not in array
    }

    // publish data on receive queue, if data has received
    if (frame.len > 0)
    {
        xQueueSend(receive_queue, &frame, portMAX_DELAY);
    }
    else
    {
        Serial.println("No data");
    }
}

// task to receive task, should receive a void pointer
void receive_task(void *parameter)
{
    dali_frame_t element;

    while (true)
    {
        xQueueReceive(receive_queue, &element, portMAX_DELAY);
        if (debug_rx)
        {
            // TODO : add print

            Serial.print("Receive: len: ");
            Serial.print(element.len);
            Serial.print(":");
            for (int j = 0; j < element.len; j++)
            {
                Serial.print(" 0x");
                Serial.print(element.bytes[j], HEX);
            }
            Serial.println();
        }

        // send received data to appropriated receivers
        dali_comm->inform_queues(element);
        // {
        //     dali_drivers[i].process_frame(element);
        // }
    }
}

// task to transmit, should receive a void point from task creation
void transmit_task(void *parameter)
{
    unsigned long last_trasmission = 0;
    dali_frame_t element;
    rmt_data_t data_array[MAX_BACKWARD_BITS];

    ESP_LOGI(TAG, "DALI Transmit task started!");

    while (true)
    {
        xQueueReceive(transmit_queue, &element, portMAX_DELAY);

        if (debug_tx)
        {
            ESP_LOGI(TAG, "Dali transmit frame received!");
            Serial.print("Transmit: len:");
            Serial.print(element.len);
            Serial.print(":");
            for (int j = 0; j < element.len; j++)
            {
                Serial.print(" 0x");
                Serial.print(element.bytes[j], HEX);
            }
            Serial.println();
        }
        if (millis() - last_trasmission < MIN_TRANSMISSION_INTERVAL)
        {
            // ESP_LOGW("Last tranmission to close, skipping transmission");
            continue;
        }

        last_trasmission = millis();

        // start bit
        data_array[0].level0 = ONE_FIRST_HALF;
        data_array[0].duration0 = DALI_HALF_BIT_TIME_US;
        data_array[0].level1 = ONE_SECOND_HALF;
        data_array[0].duration1 = DALI_HALF_BIT_TIME_US;

        int bit_idx = 1; // to control the index of the bits

        // data bits
        for (int i = 0; i < element.len; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                data_array[bit_idx].level0 = ((element.bytes[i] >> (7 - j)) & 0x01) ? ONE_FIRST_HALF : ZERO_FIRST_HALF;
                data_array[bit_idx].duration0 = DALI_HALF_BIT_TIME_US;
                data_array[bit_idx].level1 = ((element.bytes[i] >> (7 - j)) & 0x01) ? ONE_SECOND_HALF : ZERO_SECOND_HALF;
                data_array[bit_idx].duration1 = DALI_HALF_BIT_TIME_US;
                bit_idx++;
            }
        }

        // stop bits
        for (int i = 0; i < 2; i++)
        {
            data_array[bit_idx].level0 = ONE_SECOND_HALF;
            data_array[bit_idx].duration0 = DALI_HALF_BIT_TIME_US;
            data_array[bit_idx].level1 = ONE_SECOND_HALF;
            data_array[bit_idx].duration1 = DALI_HALF_BIT_TIME_US;
            bit_idx++;
        }

        // String text = "Writing data: number of bits: ";
        // text += String(bit_idx);
        // text += ": ";
        // for (int i = 0; i < bit_idx; i++)
        // {
        //     text += data_array[i].level0 == 0 ? "_" : "-";
        //     text += data_array[i].level1 == 0 ? "_|" : "-|";
        // }
        // ESP_LOGD(TAG, "%s", text.c_str());

        // Send the data
        rmtWrite(dali_comm->rmt_trans, data_array, bit_idx);
        delay(9);
        if (debug_tx)
        {
            ESP_LOGD(TAG, "Transmit complete");
        }
    } // end of while
}

DALI_COMM::DALI_COMM(int dali_rx_pin, int dali_tx_pin, UBaseType_t task_tx_priority, UBaseType_t task_rx_priority,
                     bool dali_tx_idle_level, int dali_tx_stack_size, int dali_rx_tack_size)
{
    ESP_LOGI(TAG, "Initializa dali communication");
    ESP_LOGD(TAG, "Creating queues...");

    _task_rx_priority = task_rx_priority;
    _task_tx_priority = task_tx_priority;
    // Creation of the receive queue
    receive_queue = xQueueCreate(MAX_RECEIVE_QUEUE, sizeof(dali_frame_t));
    transmit_queue = xQueueCreate(MAX_TRANSMIT_QUEUE, sizeof(dali_frame_t));

    if (receive_queue == NULL)
    {
        ESP_LOGE(TAG, "Error creating the receive queue");
    }
    if (transmit_queue == NULL)
    {
        ESP_LOGE(TAG, "Error creating the transmit queue");
    }
    ESP_LOGD(TAG, "Setup HW for receiving");
    // call setup for the receiving HW

    // setup receive dali related hardware - test were performed to fin some of the magic number

    // Initialize the channel to capture up to 192 items
    if ((rmt_recv = rmtInit(dali_rx_pin, false, RMT_MEM_192)) == NULL)
    {
        ESP_LOGE(TAG, "init receiver failed");
    }

    // Setup 10us tick
    float realTick = rmtSetTick(rmt_recv, 10000); // 10us seconds seems to work ok and give enough precison on the timings

    ESP_LOGD(TAG, "Real tick set to: %fns", realTick);
    rmtSetFilter(rmt_recv, true, 10); // filter on RX enable to dischard small pulses, probably due to slow transition speed of DALI signals
    // Ask to start reading
    rmtRead(rmt_recv, receive_data, NULL);

    // setup receive dali related hardware - test were performed to fin some of the magic number

    // pinMode(dali_tx_pin, OUTPUT);
    // digitalWrite(dali_tx_pin, HIGH);

    // Initialize the channel to capture up to 192 items
    if ((rmt_trans = rmtInit(dali_tx_pin, true, RMT_MEM_192)) == NULL)
    {
        ESP_LOGE(TAG, "init Transceiver failed");
    }

    // set dali idle level
    // for (int k = 0; k < 8; k++)
    // {
    int k = 3;
    esp_err_t error = rmt_set_idle_level((rmt_channel_t)k, true, dali_tx_idle_level ? RMT_IDLE_LEVEL_HIGH : RMT_IDLE_LEVEL_LOW);

    if (error != ESP_OK)
    {
        Serial.print("Set tx level idle not ok");
    }
    // }

    toogle_debug_pin();

    realTick = rmtSetTick(rmt_trans, 1000); // 1 us
    ESP_LOGD(TAG, "real tick set to: %fns", realTick);

    ESP_LOGD(TAG, "Creating receiving task");
    // start receive task
    xTaskCreate(
        receive_task,      /* Task function. */
        "ReceiveTask",     /* String with name of task. */
        dali_rx_tack_size, /* Stack size in bytes. */
        NULL,              /* Parameter passed as input of the task */
        _task_rx_priority, /* Priority of the task. */
        NULL);             /* Task handle. */

    ESP_LOGD(TAG, "Creating transmitting task");
    // start receive task
    xTaskCreate(
        transmit_task,      /* Task function. */
        "transmitTask",     /* String with name of task. */
        dali_tx_stack_size, /* Stack size in bytes. */
        NULL,               /* Parameter passed as input of the task */
        _task_tx_priority,  /* Priority of the task. */
        NULL);              /* Task handle. */

    ESP_LOGI(TAG, "DALI Initialization completed!");

    // TODO: to be refactored, drivers should have their own task
    // Serial.println("Create Dali Driver instances");
    // dali_drivers[0] = DaliDriver();
    // current_dali_count++;
}

void DALI_COMM::register_queue(QueueHandle_t *new_queue)
{
    if (queue_number < MAX_DALI_COMM_QUEUES)
    {
        ESP_LOGI(TAG, "Adding a new queue to receive DALI frames");
        queue_array[queue_number] = new_queue;
        queue_number++;
    }
    else
    {
        ESP_LOGE(TAG, "Not possible to add more queues!");
    }
}

void DALI_COMM::inform_queues(dali_frame_t frame)
{
    // ESP_LOGD(TAG, "DALI COM Inform queues!");
    for (int i = 0; i < queue_number; i++)
    {
        xQueueSend(*queue_array[i], &frame, portMAX_DELAY);
    }
}