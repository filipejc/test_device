#ifndef DRIVER_SERVER_H
#define DRIVER_SERVER_H

#ifdef USE_SERVER
#include "ESPAsyncWebServer.h"

// module to handle communications with the server and wifi
// so it is possible to increase the priority for this tasks

// Socket for console communications
// FIXME: give a better name
extern AsyncWebSocket ws;
extern AsyncWebSocket ws_dali; // websocket for dali

// free rtos task launched by the setup function to handle all the communication
void server_task(void *pv_parameters);

#endif
#endif