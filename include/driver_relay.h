#ifndef DRIVER_RELAY_H
#define DRIVER_RELAY_H

#ifdef USE_RELAY

class Relay
{
public:
    void set_state(bool state);
    bool get_state(void);
    Relay(int pin_number, bool state);

private:
    int _pin_number;
    bool _state;
};

#endif // USE_RELAY
#endif // DRIVER_RELAY_H