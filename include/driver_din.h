#ifndef DRIVER_IN_H
#define DRIVER_IN_H

#ifdef USE_DIGITAL
// to include IRAM_ATTR attribute
#include "esp_attr.h"

// ISR for digital in. IRAM_ATTR used to include this code in IRAM (internal RAM)
void IRAM_ATTR digital_in_ISR();

// class to interface with digital input of the device
// in case of set, it is an open collector, so it can be only guarranted to make it low.
// to make high it is necessary to have an external pull and no other active signals
class DIN
{
public:
    bool get_state(void);
    void set_state(bool);
    DIN(int read_pin_number, int set_pin_number);

private:
    int _read_pin_number;
    int _set_pin_number;
};

#endif
#endif