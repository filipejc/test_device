#ifndef DRIVER_SD_H
#define DRIVER_SD_H

#include "my_pin_number.h"
#ifdef USE_SD
#include <FS.h>
#include <SD.h>
#include <SPIFFS.h>
#include <SPI.h>
#include <Arduino.h>

#define FORMAT_SPIFFS_IF_FAILED true

void list_dir(fs::FS &fs, const char *dirname, uint8_t levels);
void create_dir(fs::FS &fs, const char *path);
void remove_dir(fs::FS &fs, const char *path);
void delete_file(fs::FS &fs, const char *path);
void rename_file(fs::FS &fs, const char *path1, const char *path2);

class MySD
{
public:
    MySD();
};

class MySPIFFS
{
public:
    MySPIFFS();
};

#endif

#endif