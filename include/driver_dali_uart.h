#ifndef DRIVER_DALI_UART_H
#define DRIVER_DALI_UART_H

#include "my_pin_number.h"

#ifdef USE_DALI_UART

#define debug_dali_uart false // if false, no prints.

extern QueueHandle_t dali_uart_receive_queue;

class DaliUart
{
public:
    DaliUart();
};
void data_uart_task(void *parameter);

#endif // USE_DALI_UART
#endif // DRIVER_DALI_UART_H