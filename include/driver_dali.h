#ifndef DRIVER_DALI_H
#define DRIVER_DALI_H

#include <Arduino.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp32-hal.h"
#include "dali_comm.h"
#include "main.h"
#include "my_pin_number.h"

#include <map>
#include <iterator>

// value for unset addresses
#define UNSET_ADDRESS -1

// define MAX and MIN
#define MIN_RANDOM_ADDRESS 0x0
#define MAX_RANDOM_ADDRESS 0xFFFFFE

#define DALI_HALF_BIT_TIME_US 417 // 1 / 1200 / 2 => 416.66 us

// define forward to backward time
const int BACKWARD_FRAME_DELAY = 14 * DALI_HALF_BIT_TIME_US / 1000; // average between maximum and minimum delay. TBC that calculation delay is ok.

const int MAX_TIME_DOUBLE_MS = 100;                  // maximum of 100ms between frames
const int MAX_INITILIATION_TIME_MS = 15 * 60 * 1000; // 15 minutes to MS

// Supported device type features
const uint8_t DEVICE_TYPE_6_FEATURES = 0xFF; // this means all features are supported

// frame responses
const uint8_t RESPONSE_YES = 0xFF; // response YES
const uint8_t RESPONSE_NO = 0x00;  // response NO
const uint8_t FRAME_MASK = 0xFF;   // response MASK

const uint8_t DEVICE_TYPE_6 = 6;                              // response for command 0x99 with device type 6
const uint8_t DEVICE_TYPES[] = {255, 6, 49, 50, 51, 52, 254}; // list of devices type. For D4i

enum CommunicationState
{
    WAITING_FORWARD_FRAME = 0,
    SENDING_BACKWARD,
    WAITING_AFTER_FORWARD_FRAME
};

const int WAITING_AFTER_FORWARD_FRAME_MS = (22 + 38) * DALI_HALF_BIT_TIME_US / 1000; // 22*TE minimum time between foward frame + 38*TE forward frame time
const int SENDING_BACKWARD_WAIT_MS = (7 + 22 + 22) * DALI_HALF_BIT_TIME_US / 1000;   // 7 * TE minimum time between forward and backward frame + 22 TE for backward time + 22 TE for backward to forward wait time.

enum InitializationState
{
    INIT_DISABLED = 0,
    INIT_ENABLED,
    INIT_WITHDRAWN
};

// needs to be extern
// Create Queues to receive and send dali frames
extern QueueHandle_t receive_queue;
extern QueueHandle_t transmit_queue;

// dali driver

#if DALI_DRIVERS_NUM > 0

// to upadate light level on web interface
void inform_light_level_change(uint16_t driver_id, uint8_t light_level);

// send a frame. to refractored
bool send_frame(dali_frame_t frame, int delay_time);

void dali_driver_task(void *parameters);

// define a class for DALI driver
class DaliDriver
{
public:
    QueueHandle_t driver_receive_queue;
    // variables
    int8_t short_address;                                    // short address, normally starts unset
    int8_t group_address;                                    // group address, normally starts unset
    uint8_t current_light_level;                             // light output
    uint8_t PHM;                                             // physical hardware minimum level
    uint8_t max_level;                                       // max light level
    uint8_t min_level;                                       // min light level
    uint8_t fade_time;                                       // fade time
    uint8_t fade_rate;                                       // fade rate
    uint8_t power_on_level;                                  // power on level
    uint8_t system_failure_level;                            // system failure level
    uint8_t module_failure_status;                           // stores the failure state
    uint8_t device_type_6_features = DEVICE_TYPE_6_FEATURES; // device type features, in the future should be dynamic
    bool lamp_failure = false;                               // lamp failure

    // functions
    DaliDriver(uint8_t id, bool D4i);
    void process_frame(dali_frame_t frame);
    void write_nvs_uint8(const char *key, uint8_t data);
    void write_nvs_uint8(const char *key, const uint8_t *data, uint8_t length);
    uint8_t read_nvs_uint8(const char *key, const uint8_t data_default);
    void read_nvs_uint8(const char *key, const uint8_t *data_default, uint8_t *array, uint8_t length);
    void power_on_load_variables();
    void factory_default_variables();
    void set_simulate_failure(bool failure);
    uint8_t get_id(); // return id

private:
    // receive queue
    uint8_t _id;                   // to store the id of the driver
    bool is_D4i;                   // store if it is a D4i driver
    uint8_t device_type_index;     // store the current index for device type
    dali_frame_t last_frame;       // store last frame received to evaluate in case of double write commands
    unsigned long last_frame_time; // received frame time
    bool is_double_frame(dali_frame_t frame);
    InitializationState initialization_state;              // store the state of initilization
    CommunicationState communication_state;                // store the communication state
    unsigned long last_comunication_state_transition_time; // store the last communication state transition time
    unsigned long initilization_timestamp_ms;              // store initilization timestamp

    bool reset_state = false; // reset state of control gear

    uint8_t random_addr_h; // random address high
    uint8_t random_addr_m; // random address middle
    uint8_t random_addr_l; // random address low

    uint8_t search_random_addr_h; // search random address high
    uint8_t search_random_addr_m; // search random address middle
    uint8_t search_random_addr_l; // search random address low

    uint8_t device_type; // store the device type to respond to application extended commands

    uint8_t DTR0; // store content of data transfer register 0
    uint8_t DTR1; // store content of data transfer register 1

    // memory bank 0
    uint8_t GTIN[6];            // store the gtin number
    uint8_t CONTROL_GEAR_FW[2]; // store the gtin number
    uint8_t SN[8];              // store the serial number
    uint8_t HW_VER[2];          // store HW version

    uint8_t last_accessible_memory_bank = 0; // Last accessible memory bank, for now, hardcoded to 0

    void set_uint8();
};

// define a class for DALI driver controller
class DaliDriverController
{
public:
    DaliDriverController();
    bool set_dali_psu_state(bool state); // set Dali PSU state
    bool get_dali_psu_state();           // get DALI PSU state
    bool create_dali_driver(uint32_t id, bool D4i);
    bool factory_reset(uint32_t id);
    bool is_id_used(uint32_t id);
    DaliDriver *get_by_id(uint32_t id);

private:
    bool dali_psu_state = false;
    std::map<uint32_t, DaliDriver *> dali_driver_array;
};

extern DaliDriverController *dali_driver_controller;
#endif // DALI_DRIVER_NUM > 0
#endif // DALI_DRIVER_H