#ifndef MY_PIN_NUMBER_H
#define MY_PIN_NUMBER_H

#include <Arduino.h>

// constant or defines that are valid for ll boards

#ifdef BOARD_PCB_V1 // board with PCB V1
// define number of simulated drivers
#define DALI_DRIVERS_NUM 1 // cannot be zero for dali decoding to work.
#define CREATE_D4i true    // set to true to create D4i driver

//#define LV_EXTEND_NAMES true
//#define USE_SCREEN
//#define USE_TOUCHSCREEN
//#define USE_1_10V_GEN
// #define USE_1_10V // implement 1_10V
#define USE_DALI // PCB V1 have dali
// #define USE_LIGHT_SENSOR  // no light sensor in V1

// gain for 1_10V feedback circuit
const float GAIN_1_10V = 0.006968895; // direct calibration between digital and output. this is the LSB value.

// constants for 1_10V operation
const int PIN_1_10V_ENABLE = 27;

// note: in reality this pin is connected to pin 4, but 4 so is connected to ADC2 that cannot be used with Wifi.
const int PIN_1_10V_FEEDBACK = 39;

// DALI pin definition
const int DALI_RX_PIN = 22;
const int DALI_TX_PIN = 25;

// Dali PSU enable
const int DALI_PSU_EN_PIN = 26;

// define pins for serial ports
const int EXTERNAL_RXD2 = 16;
const int EXTERNAL_TXD2 = 17;

// define relay pin(s)
const int RELAY_ON_OFF_PINNUMBER = 23;

// define read and set pin for digital input interface
const int DIN_READ_PINNUMBER = 13;
const int DIN_SET_PINNUMBER = 36;

// define led pin
const int LED_PINNUMBER = 12; // no LED connection

// define encoder pin numbers
const int SW_PINNUMBER = 34;
const int CLK_PINNUMBER = 39;
const int DATA_PIN_NUMBER = 36;

// define SD card pin numbers
const int SD_CS_PINNUMBER = 14;
const int SD_MOSI_PINNUMBER = 18;
const int SD_MISO_PINNUMBER = 19;
const int SD_CLK_PINNUMBER = 5;

// idle polarity for rmt the default is 0
#define DALI_TX_IDLE_POLARITY 1

// SCREEN related defines and constants
#define TFT_ADAFRUIT_2_4
const int TFT_CS = 15;
const int TFT_DC = 33;
const int STMPE_CS = 32;
const int TFT_RST = -1;

#define TFT_ROTATION 1 // Landscape orientation

#elif BOARD_PCB_DALI_USB_V1 // board with PCB V1 just with dali over USB
//#define LV_EXTEND_NAMES true
//#define USE_SCREEN
//#define USE_TOUCHSCREEN
//#define USE_1_10V_GEN
// #define USE_1_10V // implement 1_10V
// #define USE_LIGHT_SENSOR  // no light sensor in V1
#define USE_DALI            // PCB V1 have dali
#define DALI_DRIVERS_NUM 0  // cannot be zero for dali decoding to work.
#define USE_DALI_UART 1

#define MAX_UART_DALI_QUEUE 10 
// DALI pin definition
const int DALI_RX_PIN = 22;
const int DALI_TX_PIN = 25;

// Dali PSU enable
const int DALI_PSU_EN_PIN = 26;

// idle polarity for rmt the default is 0
#define DALI_TX_IDLE_POLARITY 1

#elif BOARD_M5STACK
//#define LV_EXTEND_NAMES false
#define USE_SCREEN true
#define BUTTON_A_PIN 39
#define BUTTON_B_PIN 38
#define BUTTON_C_PIN 37

//#define USE_DALI
//#define USE_TOUCHSCREEN
#define USE_BUTTONS_ENCODER
#define USE_LIGHT_SENSOR // use light sensor

// gain for 1_10V feedback circuit
const float GAIN_1_10V = 0.005085306; // direct calibration between digital and output. this is the LSB value.

// constants for 1_10V operation
// const int PIN_1_10V_ENABLE = 27;

// note: in reality this pin is connected to pin 4, but 4 so is connected to ADC2 that cannot be used with Wifi.
const int PIN_1_10V_FEEDBACK = 36;

// pin definition
const int DALI_RX_PIN = 22;
const int DALI_TX_PIN = 25;

// define pins for serial ports
const int EXTERNAL_RXD2 = 16;
const int EXTERNAL_TXD2 = 17;

// define relay pin(s)
const int RELAY_ON_OFF_PINNUMBER = 26;

// define read and set pin for digital input interface
const int DIN_READ_PINNUMBER = 5;
const int DIN_SET_PINNUMBER = 2;

// define led pin
const int LED_PINNUMBER = 12; // no LED connection

// // define encoder pin numbers
// const int SW_PINNUMBER = 34;
// const int CLK_PINNUMBER = 39;
// const int DATA_PIN_NUMBER = 36;

// define SD card pin numbers
const int SD_CS_PINNUMBER = 14;
const int SD_MOSI_PINNUMBER = 18;
const int SD_MISO_PINNUMBER = 19;
const int SD_CLK_PINNUMBER = 5;

// idle polarity for rmt the default is 0
#define DALI_TX_IDLE_POLARITY 1

// SCREEN related defines and constants
const int TFT_CS = 14;
const int TFT_DC = 27;
const int STMPE_CS = 32; // not used
const int TFT_RST = 33;

#define TFT_ROTATION 1 // Landscape orientation

#endif // Board selection
#endif // MY_PIN_NUMBER_H