#ifndef DRIVER_NVS_H
#define DRIVER_NVS_H

#ifdef USE_NVS

#include <Arduino.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp32-hal.h"

#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

const int MAX_RECEIVE_NVS_QUEUE = 10;

enum object_type
{
    NVS_UINT8 = 0

};

#define MAX_NVS_MEMORY 8
#define MAX_NVS_KEY_LENGHT 16

// definition of key tags
#define POWER_ON_LEVEL_KEY "pwr_on"
#define FAILURE_LEVEL_KEY "fail_lvl"
#define PHM_KEY "phm_lvl"
#define MIN_LEVEL_KEY "min_lvl"
#define MAX_LEVEL_KEY "max_lvl"
#define FADE_RATE_KEY "fade_rate"
#define FADE_TIME_KEY "fade_time"
#define SHORT_ADDRESS_KEY "short_addr"
#define GROUP_ADDRESS_KEY "grp_addr"
#define GTIN_KEY "GTIN"
#define FW_VERSION_KEY "fw_ver"
#define HW_VERSION_KEY "hw_ver"
#define SN_KEY "sn"

struct NVS_memory
{
    object_type NVS_TYPE;
    char key[MAX_NVS_KEY_LENGHT];
    uint8_t buffer[MAX_NVS_MEMORY];
    uint8_t length;
};

void write_nvs_task(void *parameters);

// since the object runs in several task maybe it can happens problems acessing the memory

class MyNVS
{
public:
    MyNVS();
    void save_changes();
    void set_uint8(const char *key, uint8_t data);
    uint8_t get_uint8(const char *key, uint8_t default_value);
    void set_uint8_internal(const char *key, uint8_t data);
    QueueHandle_t nvs_write_queue;

private:
    nvs_handle my_handle;
};

#endif
#endif