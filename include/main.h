#ifndef MAIN_H
#define MAIN_H

#include "driver_nvs.h"
#include "driver_screen.h"
// include 1_10v driver
#include "Driver_1_10v.h"
// include realy driver
#include "driver_relay.h"

// include dali communication
#include "dali_comm.h"

// include dali driver
#include "driver_dali.h"

// include driver for digital input
#include "driver_din.h"

// include driver for LED
#include "driver_led.h"

#ifdef USE_DALI_UART
#include "driver_dali_uart.h"
#endif // USE_DALI_UART

#ifdef USE_SERVER
#include "ESPAsyncWebServer.h"
#endif

// option to simulate light levels on
// #define SIMULATE_DALI_LIGHT_LEVELS

// define prioritites
#ifdef USE_DALI
#define DALI_COMM_TX_PRIORITY 5
#define DALI_COMM_RX_PRIORITY 6
#define DALI_TASK_PRIORITY 4
#endif // USE_DALI

#define SCREEN_TASK_PRIORITY 1
#define SERIAL_TASK_PRIORITY 2
#define DALI_SERIAL_TASK_PRIORITY 2
#define SERVER_TASK_PRIORITY 3

// define cores
#define SCREEN_TASK_CORE_ID 0

// define stack sizes
#ifdef USE_DALI
#define DALI_COMM_TX_STACK 7500
#define DALI_COMM_RX_STACK 7500
#define DALI_TASK_STACK 10000
#endif // USE_DALI

#define SCREEN_TASK_STACK 10000
#define SERIAL_TASK_STACK 5000
#define DALI_UART_TASK_STACK 5000
#define SERVER_TASK_STACK 20000

// option to debug console
// #define DEBUG_CONSOLE  // if defined, debug messages will be printed. can influenciate performance if a lot of text is printed

#ifdef USE_SCREEN

class Screen;
extern Screen *screen;
extern QueueHandle_t screen_queue;
#endif

// for dali communicatons
#ifdef USE_DALI
extern DALI_COMM *dali_comm;
#endif // USE_DALI

#ifdef USE_NVS
extern MyNVS *my_nvs;
#endif

#ifdef USE_DIGITAL
extern DIN *digital_in;
#endif

#ifdef USE_1_10V
extern Driver_1_10v *driver_1_10v;
#endif

#ifdef USE_RELAY
extern Relay *relay_on_off; // create realy global  object
#endif

#ifdef USE_LED
extern Driver_LED *photocell_led; // to driver led interface
#endif

#ifdef USE_LIGHT_SENSOR
extern LightSensor *light_sensor; // light sensor
#endif

#ifdef USE_SCREEN
// function to update screen in a thread safe way
void inform_screen_on_change_bool(ScreenMessageType msg_type, bool state);
#endif // USE_SCREEN

void print_current_task_stats();

#endif
