#ifndef DRIVER_1_10V_H
#define DRIVER_1_10V_H

#include "my_pin_number.h"

#ifdef USE_LIGHT_SENSOR
#include <Wire.h>
#include <Digital_Light_TSL2561.h>

class LightSensor
{
public:
    long read_lux();
    LightSensor();
};

#endif // USE_LIGHT_SENSOR

#ifdef USE_1_10V
class Driver_1_10v
{
public:
#ifdef USE_1_10V_GEN
    void set_state(bool state);
#endif

    bool get_state();
    float read_1_10v();
    void test();
    Driver_1_10v();

private:
    bool _state;
};
#endif

#endif