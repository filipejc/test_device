#ifndef DRIVER_LED_H
#define DRIVER_LED_H

#ifdef USE_LED

// const that define LED operation
#define PWM_LED_RESOLUTION 8
#define PWM_LED_FREQ 5000
#define PWM_LED_CHANNEL 0
// Note to create more than one LED, you need to define more that on PWM_LED_CHANNEL

class Driver_LED
{
public:
    bool set_level(int level);
    int get_level(void);
    Driver_LED(int led_pin_number);

private:
    int _led_channel;
    int _current_level;
};

#endif
#endif