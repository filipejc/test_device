#ifndef DRIVER_SCREEN_H
#define DRIVER_SCREEN_H

#include "my_pin_number.h"

#ifdef USE_SCREEN
// #include "driver_dali.h"

#define MAX_RECEIVE_SCREEN_QUEUE 10
#define MAX_MSG_TXT 20

enum MenuState
{
    MainMenu = 0,
    SecondMenu
};

enum ScreenMessageType
{
    TimeMsg = 0,
    TestMsg,
    Value1_10vMsg,
    EncoderMsg,
    UpdateLSI_SET,
    UpdateLSI_GET,
    UpdateRelay,
    Update1_10V,
    LightMsg
};

struct ScreenMessage
{
    ScreenMessageType msg_type;
    int32_t value_i32;
    float value_float;
    bool value_bool;
    String message;
    // char message[MAX_MSG_TXT];
};

// task
void screen_task(void *pvParameters);

// update screen after a modification on some parameter by other funcion
void update_screen();

// setup screen
void setup_screen();

// class Screen
// {
// public:
//     Screen();
//     void draw_triangle(uint32_t height, uint32_t pos_x, uint32_t pos_y, bool left, bool selected);
//     TFT_eSPI *tft;
// };
#endif // USE screen end
#endif // DRIVER_SCREEN_h